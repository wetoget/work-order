package com.cqt.workorder.domain;


import com.cqt.workorder.validator.Create;
import com.cqt.workorder.validator.Modify;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.Range;

import java.util.Date;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonInclude(NON_NULL)
public class SysWorkOrder {

    private String workOrderId;
    private String procInsId;
    @NotBlank(message = "workOrder.clientName.blank", groups = {Create.class})
    @Length(min = 1, max = 128, message = "workOrder.clientName.length", groups = {Create.class, Modify.class})
    private String clientName;
    @Length(max = 128, message = "workOrder.clientType.length", groups = {Create.class, Modify.class})
    private String clientType;
    @Length(max = 128, message = "workOrder.clientIdType.length", groups = {Create.class, Modify.class})
    private String clientIdType;
    @Length(min = 1, max = 128, message = "workOrder.clientIdNumber.length", groups = {Create.class, Modify.class})
    private String clientIdNumber;
    @Length(min = 1, max = 256, message = "workOrder.clientIdAddress.length", groups = {Create.class, Modify.class})
    private String clientIdAddress;

    private Date clientValidPeriod;
    @Length(min = 1, max = 64, message = "workOrder.clientCopyBusinessLicense.length", groups = {Create.class, Modify.class})
    private String clientCopyBusinessLicense;
    @Length(min = 1, max = 64, message = "workOrder.clientAccountOpeningLicense.length", groups = {Create.class, Modify.class})
    private String clientAccountOpeningLicense;
    @Length(min = 1, max = 64, message = "workOrder.clientOperatorIdImage.length", groups = {Create.class, Modify.class})
    private String clientOperatorIdImage;
    @NotBlank(message = "workOrder.clientContacts.blank", groups = {Create.class})
    @Length(min = 1, max = 128, message = "workOrder.clientContacts.length", groups = {Create.class, Modify.class})
    private String clientContacts;
    @NotBlank(message = "workOrder.clientContactsTel.blank", groups = {Create.class})
    @Length(min = 1, max = 128, message = "workOrder.clientContactsTel.length", groups = {Create.class, Modify.class})
    private String clientContactsTel;
    @NotBlank(message = "workOrder.clientEmail.blank", groups = {Create.class})
    @Length(min = 1, max = 128, message = "workOrder.clientEmail.length", groups = {Create.class, Modify.class})
    private String clientEmail;
    @Length(min = 1, max = 256, message = "workOrder.clientDeliveryAddress.length", groups = {Create.class, Modify.class})
    private String clientDeliveryAddress;
    @Length(min = 1, max = 128, message = "workOrder.bankDeposit.length", groups = {Create.class, Modify.class})
    private String bankDeposit;
    @Length(min = 1, max = 128, message = "workOrder.bankAccountName.length", groups = {Create.class, Modify.class})
    private String bankAccountName;
    @Length(min = 1, max = 128, message = "workOrder.bankAccount.length", groups = {Create.class, Modify.class})
    private String bankAccount;
    @Length(min = 1, max = 128, message = "workOrder.bankAccountFinancialContacts.length", groups = {Create.class, Modify.class})
    private String bankAccountFinancialContacts;
    @Length(min = 1, max = 128, message = "workOrder.bankAccountContactsTel.length", groups = {Create.class, Modify.class})
    private String bankAccountContactsTel;
    @Length(min = 1, max = 128, message = "workOrder.bankAccountEmail.length", groups = {Create.class, Modify.class})
    private String bankAccountEmail;
    @Length(min = 1, max = 256, message = "workOrder.bankDeliveryAddress.length", groups = {Create.class, Modify.class})
    private String bankDeliveryAddress;
    @NotBlank(message = "workOrder.slWorkOrderType.blank", groups = {Create.class})
    @Length(min = 1, max = 128, message = "workOrder.slWorkOrderType.length", groups = {Create.class, Modify.class})
    private String slWorkOrderType;
    @Length(min = 1, max = 32, message = "workOrder.slAPortProvince.length", groups = {Create.class, Modify.class})
    private String slAPortProvince;
    @Length(min = 1, max = 32, message = "workOrder.slAPortCity.length", groups = {Create.class, Modify.class})
    private String slAPortCity;
    @Length(min = 1, max = 128, message = "workOrder.slAPortType.length", groups = {Create.class, Modify.class})
    private String slAPortType;

    @NotBlank(message = "workOrder.slAPortSetupProvince.blank", groups = {Create.class})
    @Length(min = 1, max = 32, message = "workOrder.slAPortSetupProvince.length", groups = {Create.class, Modify.class})
    private String slAPortSetupProvince;
    @NotBlank(message = "workOrder.slAPortSetupCity.blank", groups = {Create.class})
    @Length(min = 1, max = 32, message = "workOrder.slAPortSetupCity.length", groups = {Create.class, Modify.class})
    private String slAPortSetupCity;
    @NotBlank(message = "workOrder.slAPortSetupCounty.blank", groups = {Create.class})
    @Length(min = 1, max = 32, message = "workOrder.slAPortSetupCounty.length", groups = {Create.class, Modify.class})
    private String slAPortSetupCounty;
    @NotBlank(message = "workOrder.slAPortSetupAddressDetail.blank", groups = {Create.class})
    @Length(min = 1, max = 256, message = "workOrder.slAPortSetupAddressDetail.length", groups = {Create.class, Modify.class})
    private String slAPortSetupAddressDetail;

    public String getSlAPortSetupProvince() {
        return slAPortSetupProvince;
    }

    public void setSlAPortSetupProvince(String slAPortSetupProvince) {
        this.slAPortSetupProvince = slAPortSetupProvince;
    }

    public String getSlAccessModel() {
        return slAccessModel;
    }

    public void setSlAccessModel(String slAccessModel) {
        this.slAccessModel = slAccessModel;
    }

    @Length(max = 128, message = "workOrder.slAccessModel.length", groups = {Create.class, Modify.class})
    private String slAccessModel;
    @Length(min = 1, max = 256, message = "workOrder.slSpecialLineName.length", groups = {Create.class, Modify.class})
    private String slSpecialLineName;
    @NotBlank(message = "workOrder.slAccessPoint.blank", groups = {Create.class})
    @Length(min = 1, max = 128, message = "workOrder.slAccessPoint.length", groups = {Create.class, Modify.class})
    private String slAccessPoint;
    @NotBlank(message = "workOrder.slSpecialLineOperators.blank", groups = {Create.class})
    @Length(min = 1, max = 128, message = "workOrder.slSpecialLineOperators.length", groups = {Create.class, Modify.class})
    private String slSpecialLineOperators;

    public String getSlAccessPortType() {
        return slAccessPortType;
    }

    public void setSlAccessPortType(String slAccessPortType) {
        this.slAccessPortType = slAccessPortType;
    }

    @NotBlank(message = "workOrder.slAccessPortType.blank", groups = {Create.class})
    @Length(min = 1, max = 128, message = "workOrder.slAccessPortType.length", groups = {Create.class, Modify.class})
    private String slAccessPortType;
    @Range(min = 2, max = 10000, message = "workOrder.slAccessBandwidth.range", groups = {Create.class, Modify.class})
    private Integer slAccessBandwidth;
    @NotBlank(message = "workOrder.slPortProvince.blank", groups = {Create.class})
    @Length(min = 1, max = 32, message = "workOrder.slPortProvince.length", groups = {Create.class, Modify.class})
    private String slPortProvince;
    @NotBlank(message = "workOrder.slPortCity.blank", groups = {Create.class})
    @Length(min = 1, max = 32, message = "workOrder.slPortCity.length", groups = {Create.class, Modify.class})
    private String slPortCity;
    @NotBlank(message = "workOrder.slPortCounty.blank", groups = {Create.class})
    @Length(min = 1, max = 32, message = "workOrder.slPortCounty.length", groups = {Create.class, Modify.class})
    private String slPortCounty;
    @NotBlank(message = "workOrder.slPortAddressDetail.blank", groups = {Create.class})
    @Length(min = 1, max = 256, message = "workOrder.slPortAddressDetail.length", groups = {Create.class, Modify.class})
    private String slPortAddressDetail;
    @Length(min = 1, max = 128, message = "workOrder.slRedundant.length", groups = {Create.class, Modify.class})
    private String slRedundant;
    @NotBlank(message = "workOrder.amName.blank", groups = {Create.class})
    @Length(min = 1, max = 128, message = "workOrder.amName.length", groups = {Create.class, Modify.class})
    private String amName;
    @NotBlank(message = "workOrder.amTel.blank", groups = {Create.class})
    @Length(min = 1, max = 128, message = "workOrder.amTel.length", groups = {Create.class, Modify.class})
    private String amTel;
    @NotBlank(message = "workOrder.amEmail.blank", groups = {Create.class})
    @Length(min = 1, max = 128, message = "workOrder.amEmail.length", groups = {Create.class, Modify.class})
    private String amEmail;
    @NotBlank(message = "workOrder.createUserName.blank", groups = {Create.class})
    @Length(min = 1, max = 128, message = "workOrder.createUserName.length", groups = {Create.class, Modify.class})
    private String createUserName;
    @NotBlank(message = "workOrder.createUserId.blank", groups = {Create.class})
    @Length(min = 1, max = 128, message = "workOrder.createUserId.length", groups = {Create.class, Modify.class})
    private String createUserId;
    private Date createTime;
    private Integer approvalStatus;
    private Integer workOrderStatus;
    private Boolean setupFeesStatus;
    private Double costSetupFees;

    public Double getCostSetupFees() {
        return costSetupFees;
    }

    public void setCostSetupFees(Double costSetupFees) {
        this.costSetupFees = costSetupFees;
    }

    public Double getCostMonthRental() {
        return costMonthRental;
    }

    public void setCostMonthRental(Double costMonthRental) {
        this.costMonthRental = costMonthRental;
    }

    public Double getCostOthers() {
        return costOthers;
    }

    public void setCostOthers(Double costOthers) {
        this.costOthers = costOthers;
    }

    public Double getIncomeSetupFees() {
        return incomeSetupFees;
    }

    public void setIncomeSetupFees(Double incomeSetupFees) {
        this.incomeSetupFees = incomeSetupFees;
    }

    public Double getIncomeMonthRental() {
        return incomeMonthRental;
    }

    public void setIncomeMonthRental(Double incomeMonthRental) {
        this.incomeMonthRental = incomeMonthRental;
    }

    public Double getIncomeOthers() {
        return incomeOthers;
    }

    public void setIncomeOthers(Double incomeOthers) {
        this.incomeOthers = incomeOthers;
    }

    private Double costMonthRental;
    private Double costOthers;
    private Double incomeSetupFees;
    private Double incomeMonthRental;
    private Double incomeOthers;

    @NotBlank(message = "workOrder.invoiceUnitName.blank", groups = {Create.class})
    @Length(min = 1, max = 128, message = "workOrder.invoiceUnitName.length", groups = {Create.class, Modify.class})
    private String invoiceUnitName;
    @NotBlank(message = "workOrder.invoiceTaxpayerId.blank", groups = {Create.class})
    @Length(min = 1, max = 128, message = "workOrder.invoiceTaxpayerId.length", groups = {Create.class, Modify.class})
    private String invoiceTaxpayerId;
    @NotBlank(message = "workOrder.invoiceUnitAddress.blank", groups = {Create.class})
    @Length(min = 1, max = 256, message = "workOrder.invoiceUnitAddress.length", groups = {Create.class, Modify.class})
    private String invoiceUnitAddress;
    @NotBlank(message = "workOrder.invoiceUnitTel.blank", groups = {Create.class})
    @Length(min = 1, max = 128, message = "workOrder.invoiceUnitTel.length", groups = {Create.class, Modify.class})
    private String invoiceUnitTel;
    @NotBlank(message = "workOrder.invoiceUnitBankDeposit.blank", groups = {Create.class})
    @Length(min = 1, max = 128, message = "workOrder.invoiceUnitBankDeposit.length", groups = {Create.class, Modify.class})
    private String invoiceUnitBankDeposit;
    @NotBlank(message = "workOrder.invoiceBankAccount.blank", groups = {Create.class})
    @Length(min = 1, max = 128, message = "workOrder.invoiceBankAccount.length", groups = {Create.class, Modify.class})
    private String invoiceBankAccount;
    @NotBlank(message = "workOrder.invoiceReceiveAddress.blank", groups = {Create.class})
    @Length(min = 1, max = 128, message = "workOrder.invoiceReceiveAddress.length", groups = {Create.class, Modify.class})
    private String invoiceReceiveAddress;

    public Integer getPacketService() {
        return packetService;
    }

    public void setPacketService(Integer packetService) {
        this.packetService = packetService;
    }

    public Integer getAgreementStatus() {
        return agreementStatus;
    }

    public void setAgreementStatus(Integer agreementStatus) {
        this.agreementStatus = agreementStatus;
    }

    private Integer packetService;
    private Integer agreementStatus;

    public String getInvoiceUnitName() {
        return invoiceUnitName;
    }

    public void setInvoiceUnitName(String invoiceUnitName) {
        this.invoiceUnitName = invoiceUnitName;
    }

    public String getInvoiceTaxpayerId() {
        return invoiceTaxpayerId;
    }

    public void setInvoiceTaxpayerId(String invoiceTaxpayerId) {
        this.invoiceTaxpayerId = invoiceTaxpayerId;
    }

    public String getInvoiceUnitAddress() {
        return invoiceUnitAddress;
    }

    public void setInvoiceUnitAddress(String invoiceUnitAddress) {
        this.invoiceUnitAddress = invoiceUnitAddress;
    }

    public String getInvoiceUnitTel() {
        return invoiceUnitTel;
    }

    public void setInvoiceUnitTel(String invoiceUnitTel) {
        this.invoiceUnitTel = invoiceUnitTel;
    }

    public String getInvoiceUnitBankDeposit() {
        return invoiceUnitBankDeposit;
    }

    public void setInvoiceUnitBankDeposit(String invoiceUnitBankDeposit) {
        this.invoiceUnitBankDeposit = invoiceUnitBankDeposit;
    }

    public String getInvoiceBankAccount() {
        return invoiceBankAccount;
    }

    public void setInvoiceBankAccount(String invoiceBankAccount) {
        this.invoiceBankAccount = invoiceBankAccount;
    }

    public String getInvoiceReceiveAddress() {
        return invoiceReceiveAddress;
    }

    public void setInvoiceReceiveAddress(String invoiceReceiveAddress) {
        this.invoiceReceiveAddress = invoiceReceiveAddress;
    }


    /* 关联表属性 start */
    public Boolean getEnableStatus() {
        return enableStatus;
    }

    public void setEnableStatus(Boolean enableStatus) {
        this.enableStatus = enableStatus;
    }

    public Date getEnableTime() {
        return enableTime;
    }

    public void setEnableTime(Date enableTime) {
        this.enableTime = enableTime;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    private Boolean enableStatus;
    private Date enableTime;
    private Date dueDate;
    /* 关联表属性 end */

    public String getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(String workOrderId) {
        this.workOrderId = workOrderId;
    }

    public String getProcInsId() {
        return procInsId;
    }

    public void setProcInsId(String procInsId) {
        this.procInsId = procInsId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getClientIdType() {
        return clientIdType;
    }

    public void setClientIdType(String clientIdType) {
        this.clientIdType = clientIdType;
    }

    public String getClientIdNumber() {
        return clientIdNumber;
    }

    public void setClientIdNumber(String clientIdNumber) {
        this.clientIdNumber = clientIdNumber;
    }

    public String getClientIdAddress() {
        return clientIdAddress;
    }

    public void setClientIdAddress(String clientIdAddress) {
        this.clientIdAddress = clientIdAddress;
    }

    public Date getClientValidPeriod() {
        return clientValidPeriod;
    }

    public void setClientValidPeriod(Date clientValidPeriod) {
        this.clientValidPeriod = clientValidPeriod;
    }

    public String getClientCopyBusinessLicense() {
        return clientCopyBusinessLicense;
    }

    public void setClientCopyBusinessLicense(String clientCopyBusinessLicense) {
        this.clientCopyBusinessLicense = clientCopyBusinessLicense;
    }

    public String getClientAccountOpeningLicense() {
        return clientAccountOpeningLicense;
    }

    public void setClientAccountOpeningLicense(String clientAccountOpeningLicense) {
        this.clientAccountOpeningLicense = clientAccountOpeningLicense;
    }

    public String getClientOperatorIdImage() {
        return clientOperatorIdImage;
    }

    public void setClientOperatorIdImage(String clientOperatorIdImage) {
        this.clientOperatorIdImage = clientOperatorIdImage;
    }

    public String getClientContacts() {
        return clientContacts;
    }

    public void setClientContacts(String clientContacts) {
        this.clientContacts = clientContacts;
    }

    public String getClientContactsTel() {
        return clientContactsTel;
    }

    public void setClientContactsTel(String clientContactsTel) {
        this.clientContactsTel = clientContactsTel;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public String getClientDeliveryAddress() {
        return clientDeliveryAddress;
    }

    public void setClientDeliveryAddress(String clientDeliveryAddress) {
        this.clientDeliveryAddress = clientDeliveryAddress;
    }

    public String getBankDeposit() {
        return bankDeposit;
    }

    public void setBankDeposit(String bankDeposit) {
        this.bankDeposit = bankDeposit;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBankAccountFinancialContacts() {
        return bankAccountFinancialContacts;
    }

    public void setBankAccountFinancialContacts(String bankAccountFinancialContacts) {
        this.bankAccountFinancialContacts = bankAccountFinancialContacts;
    }

    public String getBankAccountContactsTel() {
        return bankAccountContactsTel;
    }

    public void setBankAccountContactsTel(String bankAccountContactsTel) {
        this.bankAccountContactsTel = bankAccountContactsTel;
    }

    public String getBankAccountEmail() {
        return bankAccountEmail;
    }

    public void setBankAccountEmail(String bankAccountEmail) {
        this.bankAccountEmail = bankAccountEmail;
    }

    public String getBankDeliveryAddress() {
        return bankDeliveryAddress;
    }

    public void setBankDeliveryAddress(String bankDeliveryAddress) {
        this.bankDeliveryAddress = bankDeliveryAddress;
    }

    public String getSlWorkOrderType() {
        return slWorkOrderType;
    }

    public void setSlWorkOrderType(String slWorkOrderType) {
        this.slWorkOrderType = slWorkOrderType;
    }

    public String getSlAPortProvince() {
        return slAPortProvince;
    }

    public void setSlAPortProvince(String slAPortProvince) {
        this.slAPortProvince = slAPortProvince;
    }

    public String getSlAPortCity() {
        return slAPortCity;
    }

    public void setSlAPortCity(String slAPortCity) {
        this.slAPortCity = slAPortCity;
    }

    public String getSlAPortType() {
        return slAPortType;
    }

    public void setSlAPortType(String slAPortType) {
        this.slAPortType = slAPortType;
    }

//  public String getSlAPortSetupProvince() {
//    return slAPortSetupProvince;
//  }
//
//  public void setSlAPortSetupProvince(String slAPortSetupProvince) {
//    this.slAPortSetupProvince = slAPortSetupProvince;
//  }

    public String getSlAPortSetupCity() {
        return slAPortSetupCity;
    }

    public void setSlAPortSetupCity(String slAPortSetupCity) {
        this.slAPortSetupCity = slAPortSetupCity;
    }

    public String getSlAPortSetupCounty() {
        return slAPortSetupCounty;
    }

    public void setSlAPortSetupCounty(String slAPortSetupCounty) {
        this.slAPortSetupCounty = slAPortSetupCounty;
    }

    public String getSlAPortSetupAddressDetail() {
        return slAPortSetupAddressDetail;
    }

    public void setSlAPortSetupAddressDetail(String slAPortSetupAddressDetail) {
        this.slAPortSetupAddressDetail = slAPortSetupAddressDetail;
    }


    public String getSlSpecialLineName() {
        return slSpecialLineName;
    }

    public void setSlSpecialLineName(String slSpecialLineName) {
        this.slSpecialLineName = slSpecialLineName;
    }

    public String getSlAccessPoint() {
        return slAccessPoint;
    }

    public void setSlAccessPoint(String slAccessPoint) {
        this.slAccessPoint = slAccessPoint;
    }

    public String getSlSpecialLineOperators() {
        return slSpecialLineOperators;
    }

    public void setSlSpecialLineOperators(String slSpecialLineOperators) {
        this.slSpecialLineOperators = slSpecialLineOperators;
    }

    public Integer getSlAccessBandwidth() {
        return slAccessBandwidth;
    }

    public void setSlAccessBandwidth(Integer slAccessBandwidth) {
        this.slAccessBandwidth = slAccessBandwidth;
    }

    public String getSlPortProvince() {
        return slPortProvince;
    }

    public void setSlPortProvince(String slPortProvince) {
        this.slPortProvince = slPortProvince;
    }

    public String getSlPortCity() {
        return slPortCity;
    }

    public void setSlPortCity(String slPortCity) {
        this.slPortCity = slPortCity;
    }

    public String getSlPortCounty() {
        return slPortCounty;
    }

    public void setSlPortCounty(String slPortCounty) {
        this.slPortCounty = slPortCounty;
    }

    public String getSlPortAddressDetail() {
        return slPortAddressDetail;
    }

    public void setSlPortAddressDetail(String slPortAddressDetail) {
        this.slPortAddressDetail = slPortAddressDetail;
    }

    public String getSlRedundant() {
        return slRedundant;
    }

    public void setSlRedundant(String slRedundant) {
        this.slRedundant = slRedundant;
    }

    public String getAmName() {
        return amName;
    }

    public void setAmName(String amName) {
        this.amName = amName;
    }

    public String getAmTel() {
        return amTel;
    }

    public void setAmTel(String amTel) {
        this.amTel = amTel;
    }

    public String getAmEmail() {
        return amEmail;
    }

    public void setAmEmail(String amEmail) {
        this.amEmail = amEmail;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(Integer approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public Integer getWorkOrderStatus() {
        return workOrderStatus;
    }

    public void setWorkOrderStatus(Integer workOrderStatus) {
        this.workOrderStatus = workOrderStatus;
    }

    public Boolean getSetupFeesStatus() {
        return setupFeesStatus;
    }

    public void setSetupFeesStatus(Boolean setupFeeStatus) {
        this.setupFeesStatus = setupFeeStatus;
    }

}
