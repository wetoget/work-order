package com.cqt.workorder.domain;


import com.fasterxml.jackson.annotation.JsonInclude;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.util.Date;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonInclude(NON_NULL)
public class SysDealLog {

    private String dealId;
    private String workOrderId;
    private Integer orderStatus;
    private String remark;
    private String dealUserName;
    private String dealUserId;
    private Date createTime;


    public String getFromFlowId() {
        return fromFlowId;
    }

    public void setFromFlowId(String fromFlowId) {
        this.fromFlowId = fromFlowId;
    }

    public Integer getFromStatus() {
        return fromStatus;
    }

    public void setFromStatus(Integer fromStatus) {
        this.fromStatus = fromStatus;
    }

    public String getToFlowId() {
        return toFlowId;
    }

    public void setToFlowId(String toFlowId) {
        this.toFlowId = toFlowId;
    }

    public Integer getToStatus() {
        return toStatus;
    }

    public void setToStatus(Integer toStatus) {
        this.toStatus = toStatus;
    }

    private String fromFlowId;
    private Integer fromStatus;
    private String toFlowId;
    private Integer toStatus;
    public Double getCostSetupFees() {
        return costSetupFees;
    }

    public void setCostSetupFees(Double costSetupFees) {
        this.costSetupFees = costSetupFees;
    }

    public Double getCostMonthRental() {
        return costMonthRental;
    }

    public void setCostMonthRental(Double costMonthRental) {
        this.costMonthRental = costMonthRental;
    }

    public Double getCostOthers() {
        return costOthers;
    }

    public void setCostOthers(Double costOthers) {
        this.costOthers = costOthers;
    }

    public Double getIncomeSetupFees() {
        return incomeSetupFees;
    }

    public void setIncomeSetupFees(Double incomeSetupFees) {
        this.incomeSetupFees = incomeSetupFees;
    }

    public Double getIncomeMonthRental() {
        return incomeMonthRental;
    }

    public void setIncomeMonthRental(Double incomeMonthRental) {
        this.incomeMonthRental = incomeMonthRental;
    }

    public Double getIncomeOthers() {
        return incomeOthers;
    }

    public void setIncomeOthers(Double incomeOthers) {
        this.incomeOthers = incomeOthers;
    }

    private Double costSetupFees;
    private Double costMonthRental;
    private Double costOthers;
    private Double incomeSetupFees;
    private Double incomeMonthRental;
    private Double incomeOthers;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @NotNull(message = "workOrder.deal.status.null")
    @Range(min = 0, max = 2, message = "workOrder.deal.status.range")
    private Integer status;

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public String getWorkOrderId() {
        return workOrderId;
    }

    public void setWorkOrderId(String workOrderId) {
        this.workOrderId = workOrderId;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDealUserName() {
        return dealUserName;
    }

    public void setDealUserName(String dealUserName) {
        this.dealUserName = dealUserName;
    }

    public String getDealUserId() {
        return dealUserId;
    }

    public void setDealUserId(String dealUserId) {
        this.dealUserId = dealUserId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getPacketService() {
        return packetService;
    }

    public void setPacketService(Integer packetService) {
        this.packetService = packetService;
    }

    private Integer packetService;
}
