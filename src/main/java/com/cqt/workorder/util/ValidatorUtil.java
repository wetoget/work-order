
package com.cqt.workorder.util;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ValidatorUtil {
    public static String getErrorMessageStr(BindingResult result) {
        StringBuilder errorMessage = new StringBuilder();
        if (result.hasErrors()) {
            Iterator fieldErrorIterator = result.getFieldErrors().iterator();
            while (fieldErrorIterator.hasNext()) {
                errorMessage.append(",");
                errorMessage.append(((FieldError) fieldErrorIterator.next()).getDefaultMessage());
            }
        }
        return errorMessage.deleteCharAt(0).toString();
    }

    public static Map getErrorMessage(BindingResult result) {
        Map errorMessage = new HashMap();
        if (result.hasErrors()) {
            for (FieldError fieldError : result.getFieldErrors()) {
                errorMessage.put(fieldError.getField(), fieldError.getDefaultMessage());
            }
        }
        return errorMessage;
    }
}
