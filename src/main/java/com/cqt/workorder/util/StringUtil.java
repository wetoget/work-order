package com.cqt.workorder.util;

import org.apache.commons.lang3.StringUtils;

import java.util.UUID;

/**
 * 类名称：StringUtil   
 * 类描述：字符串处理工具类
 * @version    
 */
public class StringUtil {

	/**
	 * 隐藏密码，密码不为空时用"*"代替
	 * @param password
	 * @return
	 */
	public static String hidePassword(String password) {
		if(StringUtils.isNotBlank(password)) {
			int length = password.length();
			if(length<4){
				password = "******";
			}else {
				password = "******" + password.substring(length-3, length);
			}
		}
		return password;
	}
    /**
     * 生成UUID
     */
    public static String getUUID(){
        return UUID.randomUUID().toString();
    }
    
    /**
     * 操作符替换
     * @param operator
     * @return
     */
    public static String operatorReplace(String operator){
		String replace = "";
		if(operator.equals(">")){
			replace = "$gt";
		}else if(operator.equals(">=")){
			replace = "$gte";
		}else if(operator.equals("<")){
			replace = "$lt";
		}else if(operator.equals("<=")){
			replace = "$lte";
		}	
		return replace;
	}
    
    /**
     * 操作符替换
     * @param operator
     * @return
     */
    public static int sortReplace(String operator){
		
		if(operator.equalsIgnoreCase("desc")){
			return -1;
		}
		return 1;
	}
    
    /**
	 * 过滤特殊字符
	 * @param keyword
	 * @return
	 */
    public static String escapeExprSpecialWord(String keyword) {
		if (StringUtils.isNotBlank(keyword)) {
			String[] fbsArr = { "\\", "$", "(", ")", "*", "+", ".", "[", "]", "?", "^", "{", "}", "|" };
			for (String key : fbsArr) {
				if (keyword.contains(key)) {
//					keyword = keyword.replaceAll("\\"+key, "");
					keyword = keyword.replace(key, "\\" + key);  
				}
			}
		}
		return keyword;
	}
}
