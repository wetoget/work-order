package com.cqt.workorder.entity;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 * @program: work order
 * @description: file params
 * @author: wangmingkui
 * @create: 2018-05-18 18:10
 **/
public class FileParams {

    MultipartFile inputFile;
    String inputFileName;

    public MultipartFile getInputFile() {
        return inputFile;
    }

    public void setInputFile(MultipartFile inputFile) {
        this.inputFile = inputFile;
    }

    public String getInputFileName() {
        return inputFileName;
    }

    public void setInputFileName(String inputFileName) {
        this.inputFileName = inputFileName;
    }
}
