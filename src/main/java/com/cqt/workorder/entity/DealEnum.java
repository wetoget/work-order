package com.cqt.workorder.entity;

public enum DealEnum {
    APPROVAL_PENDING("待审核", 10,"work_order_approval"),//APPROVAL状态赋值
    APPROVAL_BACK("退回", 20, "work_order_update"),
    PASS("通过", 30, "resource_check"),//APPROVAL状态赋值

    ACCEPT_PENDING("待受理", 40, "work_order_approval"),//工单状态赋值
    BACK("退回", 50, "back_up_end_event"),
    RESOURCE_CHECKING("资源确认", 60, "resource_check"),//工单状态赋值 60，70同为资源确认关卡
    RESOURCE_PENDING("资源等待", 70, "resource_check"),//工单状态赋值
    PACKET_SERVICE_SELECTED("套餐选择", 80, "packet_service_selected"),
    OPERATOR_CHECK("运营商审核", 90, "operator_check"),
    USER_INFO_MODIFY("用户信息修改", 100, "user_info_modify"),
    SETUP_FEES_PAYMENT("初装费支付", 110, "setup_fees_payment"),
    LINE_CONSTRUCTION("线路施工", 120, "line_construction"),
    FUNCTION_TEST("联调测试", 130, "function_test"),
    DELIVERY_CHECK("交付验收", 140, "delivery_check"),
    PENDING("待处理", 150, "pending"),
    COMPLETED("已竣工", 160, "completed_end_event");

    private String eName;
    private Integer eValue;
    private String flowId;

    public String getFlowId() {
        return flowId;
    }

    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }

    public String geteName() {
        return eName;
    }

    public void seteName(String eName) {
        this.eName = eName;
    }

    public Integer geteValue() {
        return eValue;
    }

    public void seteValue(Integer eValue) {
        this.eValue = eValue;
    }

    private DealEnum(String eName, Integer eValue, String flowId) {
        this.seteName(eName);
        this.seteValue(eValue);
        this.setFlowId(flowId);
    }


}
