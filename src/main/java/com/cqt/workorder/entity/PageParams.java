package com.cqt.workorder.entity;

import com.cqt.workorder.constants.Constants;
import com.cqt.workorder.validator.PageValid;
import org.hibernate.validator.constraints.Range;

import java.io.Serializable;
import java.util.Date;

/**
 * PageParams
 */
public class PageParams implements Serializable {
    /**
     * Default serial version ID.
     */
    private static final long serialVersionUID = 14927778651357L;


    /**
     * 分页：偏移量
     */
    @Range(max = Integer.MAX_VALUE, message = "offset.illegal", groups = {PageValid.class})
    private Integer offset = Constants.DEFAULT_OFFSET;

    /**
     * 分页：单页返回值最大数量的限制
     */
    @Range(min = 1, max = Constants.DEFAULT_LIMIT_MAX, message = "limit.illegal", groups = {PageValid.class})
    private Integer limit = Constants.DEFAULT_LIMIT;

    /**
     * 是否计算总的数量
     */
    private Boolean count = Constants.DEFAULT_COUNT;

    private Integer workOrderStatus;
    private String slSpecialLineOperators;
    private Boolean setupFeesStatus;
    private Boolean enableStatus;
    private Date enabTimeStart;
    private Date enableTimeEnd;
    private String clientName;

    public Date getEnabTimeStart() {
        return enabTimeStart;
    }

    public void setEnabTimeStart(Date enabTimeStart) {
        this.enabTimeStart = enabTimeStart;
    }

    public Integer getGeWorkOrderStatus() {
        return geWorkOrderStatus;
    }

    public void setGeWorkOrderStatus(Integer geWorkOrderStatus) {
        this.geWorkOrderStatus = geWorkOrderStatus;
    }

    public Integer getAgreementStatus() {
        return agreementStatus;
    }

    public void setAgreementStatus(Integer agreementStatus) {
        this.agreementStatus = agreementStatus;
    }

    private Integer geWorkOrderStatus;
    private Integer agreementStatus;

    public String getSlSpecialLineOperators() {
        return slSpecialLineOperators;
    }

    public void setSlSpecialLineOperators(String slSpecialLineOperators) {
        this.slSpecialLineOperators = slSpecialLineOperators;
    }

    public Boolean getSetupFeesStatus() {
        return setupFeesStatus;
    }

    public void setSetupFeesStatus(Boolean setupFeesStatus) {
        this.setupFeesStatus = setupFeesStatus;
    }

    public Boolean getEnableStatus() {
        return enableStatus;
    }

    public void setEnableStatus(Boolean enableStatus) {
        this.enableStatus = enableStatus;
    }

    public Date getEnableTimeStart() {
        return enabTimeStart;
    }

    public void setEnableTimeStart(Date enableDateStart) {
        this.enabTimeStart = enableDateStart;
    }

    public Date getEnableTimeEnd() {
        return enableTimeEnd;
    }

    public void setEnableTimeEnd(Date enableDateEnd) {
        this.enableTimeEnd = enableDateEnd;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getWorkOrderStatus() {
        return workOrderStatus;
    }

    public void setWorkOrderStatus(Integer workOrderStatus) {
        this.workOrderStatus = workOrderStatus;
    }

    public Integer getOffset() {
        return offset == null ? Constants.DEFAULT_OFFSET : offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit == null ? Constants.DEFAULT_LIMIT : limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Boolean getCount() {
        return count == null ? Constants.DEFAULT_COUNT : count;
    }

    public void setCount(Boolean count) {
        this.count = count;
    }

    public Date getDueDateStart() {
        return dueDateStart;
    }

    public void setDueDateStart(Date dueDateStart) {
        this.dueDateStart = dueDateStart;
    }

    public Date getDueDateEnd() {
        return dueDateEnd;
    }

    public void setDueDateEnd(Date dueDateEnd) {
        this.dueDateEnd = dueDateEnd;
    }

    private Date dueDateStart;
    private Date dueDateEnd;
}
