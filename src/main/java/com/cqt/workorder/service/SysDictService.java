package com.cqt.workorder.service;

import com.cqt.workorder.dao.SysDictDao;
import com.cqt.workorder.domain.SysDict;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SysDictService{

    @Resource
    private SysDictDao sysDictDao;

    public int insert(SysDict pojo){
        return sysDictDao.insert(pojo);
    }

    public int insertSelective(SysDict pojo){
        return sysDictDao.insertSelective(pojo);
    }

    public int insertList(List<SysDict> pojos){
        return sysDictDao.insertList(pojos);
    }

    public int update(SysDict pojo){
        return sysDictDao.update(pojo);
    }

    SysDict findByTypeAndValue(String type, String value){
        return sysDictDao.findByTypeAndValue(type,value);
    }
}
