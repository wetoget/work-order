package com.cqt.workorder.service;

import com.cqt.workorder.dao.SysDealLogDao;
import com.cqt.workorder.domain.SysDealLog;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SysDealLogService{

    @Resource
    private SysDealLogDao sysDealLogDao;

    public int insert(SysDealLog pojo){
        return sysDealLogDao.insert(pojo);
    }

    public int insertSelective(SysDealLog pojo){
        return sysDealLogDao.insertSelective(pojo);
    }

    public int insertList(List<SysDealLog> pojos){
        return sysDealLogDao.insertList(pojos);
    }

    public int update(SysDealLog pojo){
        return sysDealLogDao.update(pojo);
    }

    public List<SysDealLog> findByWorkOrderId(String workOrderId){
        return sysDealLogDao.findByWorkOrderIdOrderByCreateTimeDesc(workOrderId);
    }


}
