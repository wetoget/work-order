package com.cqt.workorder.service;

import com.cqt.workorder.api.WorkOrderController;
import com.cqt.workorder.dao.SysWorkOrderDao;
import com.cqt.workorder.domain.*;
import com.cqt.workorder.entity.Act;
import com.cqt.workorder.entity.DealEnum;
import com.cqt.workorder.entity.PageParams;
import com.cqt.workorder.exception.CustomException;
import com.cqt.workorder.util.StringUtil;
import com.google.common.collect.Maps;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.VerticalAlignment;
import jxl.write.*;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.util.json.JSONObject;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.io.FileUtils;
import org.apache.ibatis.annotations.Param;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class SysWorkOrderService {

    @Resource
    private SysWorkOrderDao sysWorkOrderDao;

    @Autowired
    private ActTaskService actTaskService;

    @Autowired
    protected TaskService taskService;

    @Autowired
    protected SysApprovalLogService sysApprovalLogService;

    @Autowired
    protected SysDealLogService sysDealLogService;

    @Autowired
    protected SysWorkOrderCompletedService sysWorkOrderCompletedService;

    @Autowired
    private SysDictService sysDictService;

    private final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private static final Logger logger = LogManager.getLogger(WorkOrderController.class);

    public int insert(SysWorkOrder pojo) {
        return sysWorkOrderDao.insert(pojo);
    }

    public int insertSelective(SysWorkOrder pojo) {
        return sysWorkOrderDao.insertSelective(pojo);
    }

    public int insertList(List<SysWorkOrder> pojos) {
        return sysWorkOrderDao.insertList(pojos);
    }

    public int updateWorkOrder(SysWorkOrder inputWorkOrder) {
        SysWorkOrder sysWorkOrder = findByWorkOrderId(inputWorkOrder.getWorkOrderId());
        if (sysWorkOrder == null) {
            throw new CustomException(HttpStatus.NOT_FOUND, "RECORD_NOT_FOUND");
        }

        Act act = getActById(sysWorkOrder.getProcInsId());
        if (null == act.getProcIns()) {
            throw new CustomException(HttpStatus.BAD_REQUEST, "ACTIVITY.INVALID");
        }
        DealEnum nextDealEnum;
        switch (act.getProcIns().getActivityId()) {
            case "work_order_update":
                nextDealEnum = DealEnum.APPROVAL_PENDING;
                //记录审核log
                SysApprovalLog sysApprovalLog = new SysApprovalLog();
                sysApprovalLog.setApprovalId(StringUtil.getUUID());
                sysApprovalLog.setWorkOrderId(sysWorkOrder.getWorkOrderId());
                sysApprovalLog.setApprovalStatus(nextDealEnum.geteValue());
                sysApprovalLog.setApprovalUserName(sysWorkOrder.getCreateUserName());
                sysApprovalLog.setApprovalUserId(sysWorkOrder.getCreateUserId());
                sysApprovalLog.setReason("");
                sysApprovalLog.setCreateTime(new Timestamp(System.currentTimeMillis()));
                if (null == act.getProcIns()) {
                    sysApprovalLog.setFromFlowId("");
                } else {
                    sysApprovalLog.setFromFlowId(act.getProcIns().getActivityId());
                }
                sysApprovalLog.setFromStatus(sysWorkOrder.getApprovalStatus());
                sysApprovalLog.setToFlowId(nextDealEnum.getFlowId());
                sysApprovalLog.setToStatus(nextDealEnum.geteValue());

                sysApprovalLogService.insertSelective(sysApprovalLog);

                actTaskService.complete(act.getTaskId(), act.getProcInsId(),
                        "", "工单修改提交", null);
                inputWorkOrder.setApprovalStatus(nextDealEnum.geteValue());
                break;
            case "user_info_modify":
                nextDealEnum = DealEnum.OPERATOR_CHECK;
                SysDealLog sysDealLog = new SysDealLog();
                sysDealLog.setWorkOrderId(sysWorkOrder.getWorkOrderId());
                sysDealLog.setOrderStatus(DealEnum.OPERATOR_CHECK.geteValue());
                sysDealLog.setDealUserName(sysWorkOrder.getCreateUserName());
                sysDealLog.setDealUserId(sysWorkOrder.getCreateUserId());
                sysDealLog.setRemark("");
                insertDealLog(act, sysWorkOrder, sysDealLog, nextDealEnum);
                //checkWorkOrder(act, sysWorkOrder, sysDealLog, sysWorkOrder.getWorkOrderId(), nextDealEnum);
                actTaskService.complete(act.getTaskId(), act.getProcInsId(),
                        "", "用户信息修改提交", null);
                inputWorkOrder.setWorkOrderStatus(nextDealEnum.geteValue());
                inputWorkOrder.setAgreementStatus(DealEnum.OPERATOR_CHECK.geteValue());
                break;
            default:
                throw new CustomException(HttpStatus.BAD_REQUEST, "ACTIVITY.INVALID");
        }

        return update(inputWorkOrder);
    }

    public SysWorkOrder deleteWorkOrder(String workOrderId) {
        SysWorkOrder sysWorkOrder = findByWorkOrderId(workOrderId);
        if (sysWorkOrder == null) {
            throw new CustomException(HttpStatus.NOT_FOUND, "RECORD_NOT_FOUND");
        }
        if (sysWorkOrder.getApprovalStatus().equals(DealEnum.APPROVAL_PENDING.geteValue())) {
            sysWorkOrderDao.deleteByWorkOrderId(workOrderId);
        } else {
            throw new CustomException(HttpStatus.BAD_REQUEST, "RECORD_CAN_NOT_DELETE");
        }

        return sysWorkOrder;
    }

    public int update(SysWorkOrder inputWorkOrder) {
        return sysWorkOrderDao.update(inputWorkOrder);
    }

    public SysWorkOrder findByWorkOrderId(String workOrderId) {
        return sysWorkOrderDao.findByWorkOrderId(workOrderId);
    }

    public List<SysWorkOrder> find(Integer offset, Integer limit) {
        return sysWorkOrderDao.find(offset, limit);
    }

    public List<SysWorkOrder> findByPageParams(@Param("pageParams") PageParams pageParams) {
        return sysWorkOrderDao.findByPageParams(pageParams);
    }

    public Integer countByPageParams(PageParams pageParams) {
        return sysWorkOrderDao.countByPageParams(pageParams);
    }

    public Act getActById(String procInsId) {
        Act act = new Act();
        try {
            if (org.apache.commons.lang3.StringUtils.isNoneBlank(procInsId)) {
                ProcessInstance pi = actTaskService.getProcIns(procInsId);
                act.setProcInsId(procInsId);
                act.setProcIns(pi);
                if (pi != null) {
                    act.setProcDefId(pi.getProcessDefinitionId());
                    String activitiId = (String) PropertyUtils.getProperty(pi, "activityId");
                    Task task = taskService.createTaskQuery().processInstanceId(pi.getId()).taskDefinitionKey(activitiId).singleResult();
                    act.setTask(task);
                    if (task != null) {
                        act.setTaskId(task.getId());
                        act.setTaskName(task.getName());
                        act.setTaskDefKey(task.getTaskDefinitionKey());
                        act.setAssigneeName(task.getAssignee());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return act;
    }

    public SysWorkOrder createWorkOrder(SysWorkOrder inputWorkOrder) throws Exception {
        //工单ID：指承启通系统自动生成的工单ID，工单ID为唯一不可重复，生成规则为：订单创建时间年月日时分秒+后面随机4位数字，如201812391345401908；
        //使用默认时区和语言环境获得一个日历。
        Calendar rightNow = Calendar.getInstance();
            /*用Calendar的get(int field)方法返回给定日历字段的值。
            HOUR 用于 12 小时制时钟 (0 - 11)，HOUR_OF_DAY 用于 24 小时制时钟。*/
        Integer year = rightNow.get(Calendar.YEAR);
        String month = String.valueOf(rightNow.get(Calendar.MONTH) + 1); //第一个月从0开始，所以得到月份＋1
        String day = String.valueOf(rightNow.get(rightNow.DAY_OF_MONTH));
        String hour = String.valueOf(rightNow.get(rightNow.HOUR_OF_DAY));
        String minute = String.valueOf(rightNow.get(rightNow.MINUTE));
        String second = String.valueOf(rightNow.get(rightNow.SECOND));
        if (month.length() == 1) {
            month = "0" + month;
        }
        if (day.length() == 1) {
            day = "0" + day;
        }
        if (hour.length() == 1) {
            hour = "0" + hour;
        }
        if (minute.length() == 1) {
            minute = "0" + minute;
        }
        if (second.length() == 1) {
            second = "0" + second;
        }
        int randomNum = (int) (Math.random() * 9000) + 1000;
        String WordOrderId = year.toString() + month + day + hour + minute + second + randomNum;
        inputWorkOrder.setWorkOrderId(WordOrderId);
        inputWorkOrder.setApprovalStatus(DealEnum.APPROVAL_PENDING.geteValue());//默认 待审核
        inputWorkOrder.setWorkOrderStatus(DealEnum.ACCEPT_PENDING.geteValue());//默认 待受理
        inputWorkOrder.setAgreementStatus(DealEnum.OPERATOR_CHECK.geteValue());//默认 运营商待审核
        Timestamp curTime = new Timestamp(System.currentTimeMillis());
        inputWorkOrder.setCreateTime(curTime);
        String procInsId = actTaskService.startProcess("flow_work_order", "sys_work_order", WordOrderId);
        inputWorkOrder.setProcInsId(procInsId);
        insertSelective(inputWorkOrder);
        return inputWorkOrder;
    }

    public SysWorkOrder approvalWorkOrder(SysApprovalLog approvalLog, String workOrderId) {
        SysWorkOrder sysWorkOrder = findByWorkOrderId(workOrderId);
        if (sysWorkOrder == null) {
            throw new CustomException(HttpStatus.NOT_FOUND, "RECORD_NOT_FOUND");
        }
        Act act = getActById(sysWorkOrder.getProcInsId());
        String comment = "";
        Integer approvalStatus = 0;
        SysWorkOrder oWorkOrder = sysWorkOrder;
        DealEnum nextDealEnum;
        DealEnum nextApprovalDealEnum;
        switch (act.getProcIns().getActivityId()) {
            case "work_order_approval":
                Map<String, Object> vars = Maps.newHashMap();
                vars.put("pass", approvalLog.getStatus());
                if (null != approvalLog.getReason()) {
                    comment += approvalLog.getReason();
                }
                actTaskService.complete(act.getTaskId(), act.getProcInsId(),
                        comment, "审核", vars);
                Timestamp curTime = new Timestamp(System.currentTimeMillis());
                if (approvalLog.getStatus().equals(1)) {
                    nextApprovalDealEnum = DealEnum.PASS;
                    approvalStatus = nextApprovalDealEnum.geteValue();
                    //记录业务处理log
                    SysDealLog sysDealLog = new SysDealLog();
                    sysDealLog.setWorkOrderId(workOrderId);
                    sysDealLog.setOrderStatus(DealEnum.ACCEPT_PENDING.geteValue());
                    sysDealLog.setDealUserName(approvalLog.getApprovalUserName());
                    sysDealLog.setDealUserId(approvalLog.getApprovalUserId());
                    sysDealLog.setRemark(comment);
                    nextDealEnum = DealEnum.RESOURCE_CHECKING;
                    insertDealLog(act, sysWorkOrder, sysDealLog, nextDealEnum);
                    sysWorkOrder.setWorkOrderStatus(DealEnum.RESOURCE_CHECKING.geteValue());
                } else {
                    nextApprovalDealEnum = DealEnum.APPROVAL_BACK;
                    approvalStatus = nextApprovalDealEnum.geteValue();
                    if (StringUtils.isEmpty(approvalLog.getReason())) {
                        throw new CustomException(HttpStatus.BAD_REQUEST, "REASON.REQUIRED");
                    }
                    //nextDealEnum = DealEnum.ACCEPT_PENDING;
                    sysWorkOrder.setWorkOrderStatus(DealEnum.ACCEPT_PENDING.geteValue());
                }

                //记录审核log
                SysApprovalLog sysApprovalLog = new SysApprovalLog();
                sysApprovalLog.setApprovalId(StringUtil.getUUID());
                sysApprovalLog.setWorkOrderId(workOrderId);
                sysApprovalLog.setApprovalStatus(approvalStatus);
                sysApprovalLog.setApprovalUserName(approvalLog.getApprovalUserName());
                sysApprovalLog.setApprovalUserId(approvalLog.getApprovalUserId());
                sysApprovalLog.setReason(comment);
                sysApprovalLog.setCreateTime(curTime);
                if (null == act.getProcIns()) {
                    sysApprovalLog.setFromFlowId("");
                } else {
                    sysApprovalLog.setFromFlowId(act.getProcIns().getActivityId());
                }
                sysApprovalLog.setFromStatus(oWorkOrder.getApprovalStatus());
                sysApprovalLog.setToFlowId(nextApprovalDealEnum.getFlowId());
                sysApprovalLog.setToStatus(nextApprovalDealEnum.geteValue());
                sysApprovalLogService.insertSelective(sysApprovalLog);

                sysWorkOrder.setApprovalStatus(approvalStatus);
                update(sysWorkOrder);
                break;
            default:
                throw new CustomException(HttpStatus.BAD_REQUEST, "ACTIVITY.INVALID");
        }

        return sysWorkOrder;
    }

    public SysWorkOrder dealWorkOrder(SysDealLog sysDealLog, String workOrderId) throws ParseException {
        SysWorkOrder sysWorkOrder = findByWorkOrderId(workOrderId);
        if (sysWorkOrder == null) {
            throw new CustomException(HttpStatus.NOT_FOUND, "RECORD_NOT_FOUND");
        }
        Act act = getActById(sysWorkOrder.getProcInsId());

        if (null == act.getProcIns()) {
            throw new CustomException(HttpStatus.BAD_REQUEST, "ACTIVITY.INVALID");
        }
        DealEnum dealEnum=null;
        switch (act.getProcIns().getActivityId()) {
            case "resource_check":
                switch (sysDealLog.getStatus()) {
                    case 1:
                        dealEnum = DealEnum.PACKET_SERVICE_SELECTED;
                        if (null == sysDealLog.getCostSetupFees()) {
                            throw new CustomException(HttpStatus.BAD_REQUEST, "COSTSETUPFEES.REQUIRED");
                        }
                        if (null == sysDealLog.getCostMonthRental()) {
                            throw new CustomException(HttpStatus.BAD_REQUEST, "COSTMONTHRENTAL.REQUIRED");
                        }
                        if (null == sysDealLog.getCostOthers()) {
                            throw new CustomException(HttpStatus.BAD_REQUEST, "COSTOTHERS.REQUIRED");
                        }

                        if (null == sysDealLog.getIncomeSetupFees()) {
                            throw new CustomException(HttpStatus.BAD_REQUEST, "INCOMESETUPFEES.REQUIRED");
                        }
                        if (null == sysDealLog.getIncomeMonthRental()) {
                            throw new CustomException(HttpStatus.BAD_REQUEST, "INCOMEMONTHRENTAL.REQUIRED");
                        }
                        if (null == sysDealLog.getIncomeOthers()) {
                            throw new CustomException(HttpStatus.BAD_REQUEST, "INCOMEOTHERS.REQUIRED");
                        }
                        sysWorkOrder.setCostSetupFees(sysDealLog.getCostSetupFees());
                        sysWorkOrder.setCostMonthRental(sysDealLog.getCostMonthRental());
                        sysWorkOrder.setCostOthers(sysDealLog.getCostOthers());
                        sysWorkOrder.setIncomeSetupFees(sysDealLog.getIncomeSetupFees());
                        sysWorkOrder.setIncomeMonthRental(sysDealLog.getIncomeMonthRental());
                        sysWorkOrder.setIncomeOthers(sysDealLog.getIncomeOthers());
                        sysDealLog.setOrderStatus(DealEnum.RESOURCE_CHECKING.geteValue());
                        insertDealLog(act, sysWorkOrder, sysDealLog, dealEnum);
                        checkWorkOrder(act, sysWorkOrder, sysDealLog, workOrderId, dealEnum);
                        break;
                    case 0:
                        if (StringUtils.isEmpty(sysDealLog.getRemark())) {
                            throw new CustomException(HttpStatus.BAD_REQUEST, "REMARK.REQUIRED");
                        }
                        dealEnum = DealEnum.BACK;
                        sysDealLog.setOrderStatus(DealEnum.BACK.geteValue());////经讨论这里，记录当前状态是PENDING
                        insertDealLog(act, sysWorkOrder, sysDealLog, dealEnum);
                        checkWorkOrder(act, sysWorkOrder, sysDealLog, workOrderId, dealEnum);
                        break;
                    case 2:
                        dealEnum = DealEnum.RESOURCE_PENDING;
                        sysDealLog.setOrderStatus(dealEnum.geteValue());////经讨论这里，记录当前状态是PENDING
                        insertDealLog(act, sysWorkOrder, sysDealLog, dealEnum);
                        sysWorkOrder.setWorkOrderStatus(dealEnum.geteValue());
                        update(sysWorkOrder);
                        break;
                }
                break;
            case "packet_service_selected":
                if (sysDealLog.getStatus().equals(1)) {
                    if (StringUtils.isEmpty(sysDealLog.getPacketService())) {
                        throw new CustomException(HttpStatus.BAD_REQUEST, "PACKET_SERVICE.REQUIRED");
                    }
                    dealEnum = DealEnum.OPERATOR_CHECK;
                    //sysWorkOrder.setSetupFeesStatus(true);
                    sysDealLog.setOrderStatus(DealEnum.PACKET_SERVICE_SELECTED.geteValue());
                    insertDealLog(act, sysWorkOrder, sysDealLog, dealEnum);
                    sysWorkOrder.setPacketService(sysDealLog.getPacketService());
                    checkWorkOrder(act, sysWorkOrder, sysDealLog, workOrderId, dealEnum);
                } else {
                    throw new CustomException(HttpStatus.BAD_REQUEST, "ACTIVITY.INVALID");

                }
                break;
            case "operator_check":
                switch (sysDealLog.getStatus()) {
                    case 1:
                        dealEnum = DealEnum.SETUP_FEES_PAYMENT;
                        sysDealLog.setOrderStatus(DealEnum.OPERATOR_CHECK.geteValue());
                        insertDealLog(act, sysWorkOrder, sysDealLog, dealEnum);
                        sysWorkOrder.setAgreementStatus(DealEnum.SETUP_FEES_PAYMENT.geteValue());
                        break;
                    case 0:
                        if (StringUtils.isEmpty(sysDealLog.getRemark())) {
                            throw new CustomException(HttpStatus.BAD_REQUEST, "REMARK.REQUIRED");
                        }
                        dealEnum = DealEnum.USER_INFO_MODIFY;
                        sysDealLog.setOrderStatus(dealEnum.geteValue());//经讨论这里，记录当前状态是PENDING
                        sysWorkOrder.setAgreementStatus(DealEnum.USER_INFO_MODIFY.geteValue());
                        insertDealLog(act, sysWorkOrder, sysDealLog, dealEnum);
                        break;
                    default:
                        throw new CustomException(HttpStatus.BAD_REQUEST, "ACTIVITY.INVALID");
                }
                checkWorkOrder(act, sysWorkOrder, sysDealLog, workOrderId, dealEnum);
                break;
            case "setup_fees_payment":
                if (sysDealLog.getStatus().equals(1)) {
                    dealEnum = DealEnum.LINE_CONSTRUCTION;
                    sysWorkOrder.setSetupFeesStatus(true);
                    sysDealLog.setOrderStatus(DealEnum.SETUP_FEES_PAYMENT.geteValue());
                    insertDealLog(act, sysWorkOrder, sysDealLog, dealEnum);
                    checkWorkOrder(act, sysWorkOrder, sysDealLog, workOrderId, dealEnum);
                } else {
                    throw new CustomException(HttpStatus.BAD_REQUEST, "ACTIVITY.INVALID");
                }
                break;
            case "line_construction":
                switch (sysDealLog.getStatus()) {
                    case 1:
                        dealEnum = DealEnum.FUNCTION_TEST;
                        sysDealLog.setOrderStatus(DealEnum.LINE_CONSTRUCTION.geteValue());
                        insertDealLog(act, sysWorkOrder, sysDealLog, DealEnum.FUNCTION_TEST);
                        checkWorkOrder(act, sysWorkOrder, sysDealLog, workOrderId, dealEnum);
                        break;
                    case 0:
                        sysDealLog.setOrderStatus(DealEnum.LINE_CONSTRUCTION.geteValue());
                        insertDealLog(act, sysWorkOrder, sysDealLog, DealEnum.LINE_CONSTRUCTION);
                        break;
                    default:
                        throw new CustomException(HttpStatus.BAD_REQUEST, "ACTIVITY.INVALID");
                }
                break;
            case "function_test":
                switch (sysDealLog.getStatus()) {
                    case 1:
                        dealEnum = DealEnum.DELIVERY_CHECK;
                        sysDealLog.setOrderStatus(DealEnum.FUNCTION_TEST.geteValue());
                        insertDealLog(act, sysWorkOrder, sysDealLog, DealEnum.DELIVERY_CHECK);
                        checkWorkOrder(act, sysWorkOrder, sysDealLog, workOrderId, dealEnum);
                        break;
                    case 0:
                        sysDealLog.setOrderStatus(DealEnum.FUNCTION_TEST.geteValue());
                        insertDealLog(act, sysWorkOrder, sysDealLog, DealEnum.FUNCTION_TEST);
                        break;
                    default:
                        throw new CustomException(HttpStatus.BAD_REQUEST, "ACTIVITY.INVALID");
                }
                break;
            case "delivery_check":
                switch (sysDealLog.getStatus()) {
                    case 1:
                        dealEnum = DealEnum.COMPLETED;
                        sysDealLog.setOrderStatus(DealEnum.DELIVERY_CHECK.geteValue());
                        insertDealLog(act, sysWorkOrder, sysDealLog, DealEnum.DELIVERY_CHECK);
                        break;
                    case 0:
                        if (StringUtils.isEmpty(sysDealLog.getRemark())) {
                            throw new CustomException(HttpStatus.BAD_REQUEST, "REMARK.REQUIRED");
                        }
                        dealEnum = DealEnum.PENDING;
                        sysDealLog.setOrderStatus(DealEnum.PENDING.geteValue());//经讨论这里，记录当前状态是PENDING
                        insertDealLog(act, sysWorkOrder, sysDealLog, dealEnum);
                        break;
                    default:
                        throw new CustomException(HttpStatus.BAD_REQUEST, "ACTIVITY.INVALID");
                }
                checkWorkOrder(act, sysWorkOrder, sysDealLog, workOrderId, dealEnum);
                if (sysDealLog.getStatus().equals(1)) {
                    SysWorkOrderCompleted sysWorkOrderCompleted = new SysWorkOrderCompleted();
                    sysWorkOrderCompleted.setWorkOrderId(workOrderId);
                    // 2018/6/11  计算到期时间
                    //选中套餐并确认缴费后，从工单竣工日次日起计算，相加相应套餐的月份，为该客户使用资源的有效期。
                    //例：套餐选择3个月，工单2018年6月7日竣工，则2018年6月8日至2018年9月8日为资源的有效期，若用户没有续费，2018年9月9日，该专线资源停用
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    String strDate=dateFormat.format(new Date());
                    Date dealDate=dateFormat.parse(strDate);

                    Calendar rightNow = Calendar.getInstance();
                    rightNow.setTime(dealDate);
                    rightNow.add(Calendar.DAY_OF_YEAR,2);//日期加2天
                    rightNow.add(Calendar.MONTH,sysWorkOrder.getPacketService());//日期加3个月

                    sysWorkOrderCompleted.setDueDate(rightNow.getTime());
                    sysWorkOrderCompleted.setEnableStatus(true);
                    sysWorkOrderCompleted.setEnableTime(new Timestamp(System.currentTimeMillis()));
                    sysWorkOrderCompletedService.insertSelective(sysWorkOrderCompleted);
                }
                break;
            case "pending":
                if (sysDealLog.getStatus().equals(1)) {
                    dealEnum = DealEnum.DELIVERY_CHECK;

                } else {
                    throw new CustomException(HttpStatus.BAD_REQUEST, "ACTIVITY.INVALID");
                }
                sysDealLog.setOrderStatus(DealEnum.DELIVERY_CHECK.geteValue()); //经讨论这里pending 时，记录当前状态是交付验收
                insertDealLog(act, sysWorkOrder, sysDealLog, dealEnum);
                checkWorkOrder(act, sysWorkOrder, sysDealLog, workOrderId, dealEnum);

                break;
            default:
                throw new CustomException(HttpStatus.BAD_REQUEST, "ACTIVITY.INVALID");
        }
        return sysWorkOrder;
    }

    public SysWorkOrder checkWorkOrder(Act act, SysWorkOrder sysWorkOrder, SysDealLog sysDealLog, String workOrderId, DealEnum nextDealEnum) {
        String comment = "";
        Integer dealStatus = 0;
        Map<String, Object> vars = Maps.newHashMap();
        vars.put("pass", sysDealLog.getStatus());
        if (null != sysDealLog.getRemark()) {
            comment += sysDealLog.getRemark();
        }
        actTaskService.complete(act.getTaskId(), act.getProcInsId(),
                comment, "审核", vars);
        dealStatus = nextDealEnum.geteValue();

        sysWorkOrder.setWorkOrderStatus(dealStatus);
        update(sysWorkOrder);
        return sysWorkOrder;
    }

    public SysDealLog insertDealLog(Act act, SysWorkOrder sysWorkOrder, SysDealLog sysDealLog, DealEnum nextDealEnum) {
        String comment = "";
        Map<String, Object> vars = Maps.newHashMap();
        vars.put("pass", sysDealLog.getStatus());
        if (null != sysDealLog.getRemark()) {
            comment += sysDealLog.getRemark();
        }

        //SysDealLog sysDealLog=new SysDealLog();
        sysDealLog.setDealId(StringUtil.getUUID());
        sysDealLog.setWorkOrderId(sysWorkOrder.getWorkOrderId());
        if (null == act.getProcIns()) {
            sysDealLog.setFromFlowId("");
        } else {
            sysDealLog.setFromFlowId(act.getProcIns().getActivityId());
        }

        sysDealLog.setFromStatus(sysWorkOrder.getWorkOrderStatus());
        sysDealLog.setToFlowId(nextDealEnum.getFlowId());
        sysDealLog.setToStatus(nextDealEnum.geteValue());
        sysDealLog.setDealUserName(sysDealLog.getDealUserName());
        sysDealLog.setDealUserId(sysDealLog.getDealUserId());
        sysDealLog.setRemark(comment);
        Timestamp curTime = new Timestamp(System.currentTimeMillis());
        sysDealLog.setCreateTime(curTime);

        sysDealLogService.insertSelective(sysDealLog);
        return sysDealLog;
    }

    public SysWorkOrderCompleted updateEnableStatus(SysWorkOrderCompleted sysWorkOrderCompleted, String workOrderId) {
        //SysWorkOrder sysWorkOrder = sysWorkOrderService.findByWorkOrderId(workOrderId);
        //monthRentalLog.setDueDate();
        //monthRentalLog.setPayTime();
        sysWorkOrderCompleted.setWorkOrderId(workOrderId);
        sysWorkOrderCompleted.setEnableTime(new Timestamp(System.currentTimeMillis()));
        //monthRentalLogService.findByWorkOrderIdOrderByPayTimeDesc(workOrderId);
        //monthRentalLogService.insertSelective(monthRentalLog);
        sysWorkOrderCompletedService.update(sysWorkOrderCompleted);
        return sysWorkOrderCompleted;
    }

    /**
     * @param requestParams
     * @param response
     * @return
     */
    @SuppressWarnings("rawtypes")
    public String exportExcelByPageParams(PageParams requestParams, HttpServletResponse response) {

        requestParams.setLimit(90000);//@todo 第一版先这样，后面优化
        requestParams.setWorkOrderStatus(DealEnum.COMPLETED.geteValue());
        List<SysWorkOrder> sysWorkOrders = findByPageParams(requestParams);

        // 导出参数
        String fileName = "客户信息.xls";
        String result = "success";
        // 以下开始输出到EXCEL
        try {
            // 定义输出流，以便打开保存对话框______________________begin
            // HttpServletResponse response=ServletActionContext.getResponse();
            OutputStream os = response.getOutputStream();// 取得输出流
            response.reset();// 清空输出流
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setHeader("Content-disposition",
                    "attachment; filename=" + new String(fileName.getBytes("GB2312"), "ISO8859-1"));
            // 设定输出文件头
            response.setContentType("application/msexcel");// 定义输出类型
            // 定义输出流，以便打开保存对话框_______________________end

            /** **********创建工作簿************ */
            WritableWorkbook workbook = Workbook.createWorkbook(os);

            /** **********创建工作表************ */

            WritableSheet sheet = workbook.createSheet("Sheet1", 0);

            /** **********设置纵横打印（默认为纵打）、打印纸***************** */
            jxl.SheetSettings sheetset = sheet.getSettings();
            sheetset.setProtected(false);

            /** ************设置单元格字体************** */
            WritableFont NormalFont = new WritableFont(WritableFont.ARIAL, 10);
            WritableFont BoldFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);

            /** ************以下设置三种单元格样式，灵活备用************ */
            // 用于标题居中
            WritableCellFormat wcf_center = new WritableCellFormat(BoldFont);
            wcf_center.setBorder(Border.ALL, BorderLineStyle.THIN); // 线条
            wcf_center.setVerticalAlignment(VerticalAlignment.CENTRE); // 文字垂直对齐
            wcf_center.setAlignment(Alignment.CENTRE); // 文字水平对齐
            wcf_center.setWrap(false); // 文字是否换行
            wcf_center.setShrinkToFit(true);
            // 用于正文居左
            WritableCellFormat wcf_left = new WritableCellFormat(NormalFont);
            wcf_left.setBorder(Border.ALL, BorderLineStyle.THIN); // 线条
            wcf_left.setVerticalAlignment(VerticalAlignment.CENTRE); // 文字垂直对齐
            wcf_left.setAlignment(Alignment.LEFT); // 文字水平对齐
            wcf_left.setWrap(false); // 文字是否换行
            //wcf_left.setShrinkToFit(true);

            /** ***************以下是EXCEL开头大标题，暂时省略********************* */
            // sheet.mergeCells(0, 0, colWidth, 0);
            // sheet.addCell(new Label(0, 0, "XX报表", wcf_center));
            /** ***************以下是EXCEL第一行列标题********************* */

            sheet.addCell(new Label(0, 0, "序号", wcf_center));
            int h = 0;
            sheet.addCell(new Label(++h, 0, "工单ID", wcf_center));
            sheet.setColumnView(h, 20);
            sheet.addCell(new Label(++h, 0, "客户名称", wcf_center));
            sheet.setColumnView(h, 20);
            sheet.addCell(new Label(++h, 0, "接入类型", wcf_center));
            sheet.setColumnView(h, 20);
            sheet.addCell(new Label(++h, 0, "专线名称", wcf_center));
            sheet.setColumnView(h, 20);
            sheet.addCell(new Label(++h, 0, "使用运营商", wcf_center));
            sheet.setColumnView(h, 20);
            sheet.addCell(new Label(++h, 0, "接入端口类型", wcf_center));
            sheet.setColumnView(h, 30);
            sheet.addCell(new Label(++h, 0, "专线对端地址", wcf_center));
            sheet.setColumnView(h, 30);
            sheet.addCell(new Label(++h, 0, "联系人", wcf_center));
            sheet.setColumnView(h, 20);
            sheet.addCell(new Label(++h, 0, "联系电话", wcf_center));
            sheet.setColumnView(h, 20);
            sheet.addCell(new Label(++h, 0, "启用时间", wcf_center));
            sheet.setColumnView(h, 20);
            sheet.addCell(new Label(++h, 0, "状态", wcf_center));
            sheet.setColumnView(h, 20);
            sheet.addCell(new Label(++h, 0, "到期时间", wcf_center));
            sheet.setColumnView(h, 20);
            /** ***************以下是EXCEL正文数据********************* */
            Integer i = 1;
            for (SysWorkOrder sysWorkOrder : sysWorkOrders) {
                int j = 0;
                sheet.addCell(new Label(0, i, i.toString(), wcf_left));
                sheet.addCell(new Label(++j, i, sysWorkOrder.getWorkOrderId(), wcf_left));
                sheet.addCell(new Label(++j, i, sysWorkOrder.getClientName(), wcf_left));
                String slAccessModel = "-";
                if (null != sysWorkOrder.getSlAccessModel()) {
                    slAccessModel = sysWorkOrder.getSlAccessModel();//VPC没有字典
                }
                sheet.addCell(new Label(++j, i, slAccessModel, wcf_left));

                String slSpecialLineName = "-";
                if (null != sysWorkOrder.getSlSpecialLineName()) {
                    slSpecialLineName = sysWorkOrder.getSlSpecialLineName();
                }
                sheet.addCell(new Label(++j, i, slSpecialLineName, wcf_left));

                SysDict sysDict1 = sysDictService.findByTypeAndValue("sl_special_line_operators", sysWorkOrder.getSlSpecialLineOperators());
                sheet.addCell(new Label(++j, i, sysDict1.getLabel(), wcf_left));
                SysDict sysDict2 = sysDictService.findByTypeAndValue("sl_access_port_type", sysWorkOrder.getSlAccessPortType());
                String slAccessPortTypeLabel="";
                if(sysDict2!=null){
                    slAccessPortTypeLabel=sysDict2.getLabel();
                }
                sheet.addCell(new Label(++j, i, slAccessPortTypeLabel, wcf_left));
                String addressDetail = "";
                try {
                    if (null != sysWorkOrder.getSlPortProvince()) {
                        File jsonFile = ResourceUtils.getFile("classpath:address.json");
                        String content = FileUtils.readFileToString(jsonFile, "UTF-8");
                        JSONObject jsonObject = new JSONObject(content);
                        if (null != jsonObject.getJSONObject(sysWorkOrder.getSlPortProvince())) {
                            jsonObject = jsonObject.getJSONObject(sysWorkOrder.getSlPortProvince());
                            addressDetail = jsonObject.getString("name");//省
                            if (null != sysWorkOrder.getSlPortCity() && null != jsonObject.getJSONObject("child") && null != jsonObject.getJSONObject("child").getJSONObject(sysWorkOrder.getSlPortCity())) {
                                jsonObject = jsonObject.getJSONObject("child").getJSONObject(sysWorkOrder.getSlPortCity());
                                addressDetail += jsonObject.getString("name");//市
                                if (null != sysWorkOrder.getSlPortCounty() && null != jsonObject.getJSONObject("child") && null != jsonObject.getJSONObject("child").getString(sysWorkOrder.getSlPortCounty())) {
                                    addressDetail += jsonObject.getJSONObject("child").getString(sysWorkOrder.getSlPortCounty());
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    addressDetail = "";
                    logger.error("省市区解析错误", e);
                }

                addressDetail += sysWorkOrder.getSlPortAddressDetail();
                sheet.addCell(new Label(++j, i, addressDetail, wcf_left));
                sheet.addCell(new Label(++j, i, sysWorkOrder.getClientContacts(), wcf_left));
                sheet.addCell(new Label(++j, i, sysWorkOrder.getClientContactsTel(), wcf_left));
                String enableTime = dateFormat.format(sysWorkOrder.getEnableTime());
                sheet.addCell(new Label(++j, i, enableTime, wcf_left));
                String statusView = "";
                if(!sysWorkOrder.getEnableStatus()){
                    statusView = "关停";
                }else{
                    if(sysWorkOrder.getDueDate().before(new Date())){
                        statusView = "关停";
                    }else{
                        statusView = "正常";
                    }
                }
                sheet.addCell(new Label(++j, i, statusView, wcf_left));
                sheet.addCell(new Label(++j, i, dateFormat.format(sysWorkOrder.getDueDate()), wcf_left));
                ++i;
            }
            /** **********将以上缓存中的内容写到EXCEL文件中******** */
            workbook.write();
            /** *********关闭文件************* */
            workbook.close();
            logger.info("导出excel成功");

        } catch (Exception e) {
            result = "error";
            logger.error("导出excel失败", e);
        }
        return result;
    }


}
