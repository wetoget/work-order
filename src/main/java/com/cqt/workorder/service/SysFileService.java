package com.cqt.workorder.service;

import com.cqt.workorder.dao.SysFileDao;
import com.cqt.workorder.domain.SysFile;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SysFileService{

    @Resource
    private SysFileDao sysFileDao;

    public int insert(SysFile pojo){
        return sysFileDao.insert(pojo);
    }

    public int insertSelective(SysFile pojo){
        return sysFileDao.insertSelective(pojo);
    }

    public int insertList(List<SysFile> pojos){
        return sysFileDao.insertList(pojos);
    }

    public int update(SysFile pojo){
        return sysFileDao.update(pojo);
    }

    public SysFile findByDentryId(String dentryId){
        return sysFileDao.findByDentryId(dentryId);
    }
}
