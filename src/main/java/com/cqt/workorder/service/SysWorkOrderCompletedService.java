package com.cqt.workorder.service;

import com.cqt.workorder.dao.SysWorkOrderCompletedDao;
import com.cqt.workorder.domain.SysWorkOrderCompleted;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SysWorkOrderCompletedService{

    @Resource
    private SysWorkOrderCompletedDao sysWorkOrderCompletedDao;

    public int insert(SysWorkOrderCompleted pojo){
        return sysWorkOrderCompletedDao.insert(pojo);
    }

    public int insertSelective(SysWorkOrderCompleted pojo){
        return sysWorkOrderCompletedDao.insertSelective(pojo);
    }

    public int insertList(List<SysWorkOrderCompleted> pojos){
        return sysWorkOrderCompletedDao.insertList(pojos);
    }

    public int update(SysWorkOrderCompleted pojo){
        return sysWorkOrderCompletedDao.update(pojo);
    }
}
