package com.cqt.workorder.service;

import com.cqt.workorder.dao.SysApprovalLogDao;
import com.cqt.workorder.domain.SysApprovalLog;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class SysApprovalLogService{

    @Resource
    private SysApprovalLogDao sysApprovalLogDao;

    public int insert(SysApprovalLog pojo){
        return sysApprovalLogDao.insert(pojo);
    }

    public int insertSelective(SysApprovalLog pojo){
        return sysApprovalLogDao.insertSelective(pojo);
    }

    public int insertList(List<SysApprovalLog> pojos){
        return sysApprovalLogDao.insertList(pojos);
    }

    public int update(SysApprovalLog pojo){
        return sysApprovalLogDao.update(pojo);
    }

    public List<SysApprovalLog> findByWorkOrderId(String workOrderId){
        return sysApprovalLogDao.findByWorkOrderId(workOrderId);
    }
}
