package com.cqt.workorder.config;

import com.cqt.a8.rest.AbstractCafWebApplicationInitializer;



/**
 * spring启动方法 这里必须继承 控制器才能调用
 */
public class CafWebInitializer extends AbstractCafWebApplicationInitializer {

  @Override
  protected Class<?>[] getServletConfigClasses() {
    return new Class<?>[] {};
  }

  /**
   * 默认服务启动，加载的权限验证模块实现类
   */
  @Override
  protected Class<?>[] getRootConfigClasses() {
    return new Class[]{CafWebConfig.class,MysqlConfig.class};
  }

  @Override
  protected String[] getServletMappings() {
    return new String[]{"/","/activiti-explorer/"};
  }
}