package com.cqt.workorder.config;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @program: work order
 * @description: activiti config
 * @author: wangmingkui
 * @create: 2018-05-09 17:44
 **/
//@Configuration
//@ComponentScan(basePackages={"org.activiti.rest.editor"},
//        includeFilters={@ComponentScan.Filter(type=FilterType.ANNOTATION,value=Controller.class)
//        })
public class ActivitiConfig {
    //@Autowired
    //DataSourceTransactionManager dataSourceTransactionManager;
//
//    @Bean
//    public SpringProcessEngineConfiguration processEngineConfiguration(DataSource dataSource,
//                                                                       DataSourceTransactionManager dataSourceTransactionManager) {
//        //dataSource,dataSourceTransactionManager,"mysql",false
//        SpringProcessEngineConfiguration processEngineConfiguration = new SpringProcessEngineConfiguration();
//        processEngineConfiguration.setDataSource(dataSource);
//        processEngineConfiguration.setTransactionManager(dataSourceTransactionManager);
//        processEngineConfiguration.setDatabaseType("mysql");
//        processEngineConfiguration.setDatabaseSchemaUpdate("false");
//        return processEngineConfiguration;
//    }
//
//    @Bean
//    public ProcessEngineFactoryBean processEngine(SpringProcessEngineConfiguration processEngineConfiguration) {
//        ProcessEngineFactoryBean processEngine = new ProcessEngineFactoryBean();
//        processEngine.setProcessEngineConfiguration(processEngineConfiguration);
//        return processEngine;
//    }
//
//    //        <!-- 7种服务 不一定全部使用 -->
////    <bean id="repositoryService" factory-bean="processEngine" factory-method="getRepositoryService"/>
//    @Bean
//    public RepositoryService repositoryService(ProcessEngineFactoryBean processEngine) throws Exception {
//        return processEngine.getObject().getRepositoryService();
//    }
//
//    //    <bean id="runtimeService" factory-bean="processEngine" factory-method="getRuntimeService"/>
//    @Bean
//    public RuntimeService runtimeService(ProcessEngineFactoryBean processEngine) throws Exception {
//        return processEngine.getObject().getRuntimeService();
//    }
////    <bean id="formService" factory-bean="processEngine" factory-method="getFormService"/>
//
//    @Bean
//    public FormService formService(ProcessEngineFactoryBean processEngine) throws Exception {
//        return processEngine.getObject().getFormService();
//    }
//
//    //    <bean id="identityService" factory-bean="processEngine" factory-method="getIdentityService"/>
//    @Bean
//    public IdentityService identityService(ProcessEngineFactoryBean processEngine) throws Exception {
//        return processEngine.getObject().getIdentityService();
//    }
////    <bean id="taskService" factory-bean="processEngine" factory-method="getTaskService"/>
//
//    @Bean
//    public TaskService taskService(ProcessEngineFactoryBean processEngine) throws Exception {
//        return processEngine.getObject().getTaskService();
//    }
////    <bean id="historyService" factory-bean="processEngine" factory-method="getHistoryService"/>
//
//    @Bean
//    public HistoryService historyService(ProcessEngineFactoryBean processEngine) throws Exception {
//        return processEngine.getObject().getHistoryService();
//    }
//
//    //    <bean id="managementService" factory-bean="processEngine" factory-method="getManagementService"/>
//    @Bean
//    public ManagementService managementService(ProcessEngineFactoryBean processEngine) throws Exception {
//        return processEngine.getObject().getManagementService();
//    }
//
//    @Bean
//    public ObjectMapper objectMapper() {
//        return new ObjectMapper();
//    }
//
//
//    @Bean
//    public RepositoryService RepositoryService() {
//        return new RepositoryServiceImpl();
//    }


    public static void main(String[] args) {
//        if("adada>".matches("[/:*?\"<>|]+")){
//            System.out.println("true");
//            //return true;
//        }
        Pattern p = Pattern.compile("[/:*?\"<>|]+");
        Matcher m = p.matcher("df.png"); // 获取 matcher 对象
        if(m.find()) {
            System.out.println("true");
        }
    }
}
