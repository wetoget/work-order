package com.cqt.workorder.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.activiti.engine.*;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.spring.ProcessEngineFactoryBean;
import org.activiti.spring.SpringProcessEngineConfiguration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@MapperScan("com.cqt.workorder.dao")//自动扫描，就不用一个个接口来添加了
@PropertySource("classpath:rdb.properties")
@EnableTransactionManagement  //注解是为了增加事务的支持
public class MysqlConfig {
    @Value("${driverClassName}")
    private String jdbcDriver;

    @Value("${dbUsername}")
    private String username;

    @Value("${dbPassword}")
    private String password;

    @Value("${url}")
    private String jdbcUrl;

    //最小、最大
    @Value("3")
    private String minIdle;

    @Value("50")
    private String maxActive;

    @Value("${maxWait}")
    private String maxWait;     //配置获取连接等待超时的时间

    @Value("${validationQuery}")
    private String validationQuery;

    @Value("${testOnBorrow}")
    //@Value(false)
    private boolean testOnBorrow;


    @Value("${testOnReturn}")
    //@Value(false)
    private boolean testOnReturn;

    @Value("${testWhileIdle}")
    private boolean testWhileIdle;

    //配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
    @Value("${timeBetweenEvictionRunsMillis}")
    private long timeBetweenEvictionRunsMills;

    //配置一个连接在池中最小生存的时间，单位是毫秒
    @Value("${minEvictableIdleTimeMillis}")
    private long minEvictableTimeMills;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public DataSource dataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDriverClassName(this.jdbcDriver);
        dataSource.setUsername(this.username);
        dataSource.setPassword(this.password);
        dataSource.setUrl(this.jdbcUrl);
        dataSource.setMaxActive(Integer.valueOf(this.maxActive));
        dataSource.setValidationQuery(this.validationQuery);
        dataSource.setTestOnBorrow(this.testOnBorrow);
        dataSource.setTestOnReturn(this.testOnReturn);
        dataSource.setTestWhileIdle(this.testWhileIdle);
        dataSource.setTimeBetweenConnectErrorMillis(this.timeBetweenEvictionRunsMills);
        dataSource.setMinEvictableIdleTimeMillis(minEvictableTimeMills);
        dataSource.setPoolPreparedStatements(true);
        dataSource.setMaxOpenPreparedStatements(20);
        dataSource.setInitialSize(50);
        return dataSource;
    }

    @Bean
    public JdbcTemplate jdbcTemplate(DataSource dataSource) {

        return new JdbcTemplate(dataSource);
    }

    @Bean
    public DataSourceTransactionManager dataSourceTransactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }

    @Bean
    public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(sqlSessionFactory);
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory() throws Exception {
        SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        //扫描xml映射文件的路径
        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mappings/*.xml"));
        //typeAliasesPackage：它一般对应我们的实体类所在的包，这个时候会自动取对应包中不包括包名的简单类名作为包括包名的别名。多个package之间可以用逗号或者分号等来进行分隔。(value的值一定要是包的全名)
        sessionFactory.setTypeAliasesPackage("com.cqt.workorder");
        //其他配置
        //sessionFactory.setc...
        return sessionFactory.getObject();
    }

    //activiti
    @Bean
    public SpringProcessEngineConfiguration processEngineConfiguration() {
        //dataSource,dataSourceTransactionManager,"mysql",false
        SpringProcessEngineConfiguration processEngineConfiguration = new SpringProcessEngineConfiguration();
        processEngineConfiguration.setDataSource(dataSource());
        processEngineConfiguration.setTransactionManager(dataSourceTransactionManager());
        processEngineConfiguration.setDatabaseType("mysql");
        processEngineConfiguration.setDatabaseSchemaUpdate("false");
        return processEngineConfiguration;
    }

    @Bean
    public ProcessEngineFactoryBean processEngine() {
        ProcessEngineFactoryBean processEngine = new ProcessEngineFactoryBean();
        processEngine.setProcessEngineConfiguration(processEngineConfiguration());
        return processEngine;
    }

    //        <!-- 7种服务 不一定全部使用 -->
//    <bean id="repositoryService" factory-bean="processEngine" factory-method="getRepositoryService"/>
    @Bean
    public RepositoryService repositoryService() throws Exception {
        return processEngine().getObject().getRepositoryService();
    }

    //    <bean id="runtimeService" factory-bean="processEngine" factory-method="getRuntimeService"/>
    @Bean
    public RuntimeService runtimeService() throws Exception {
        return processEngine().getObject().getRuntimeService();
    }
//    <bean id="formService" factory-bean="processEngine" factory-method="getFormService"/>

    @Bean
    public FormService formService() throws Exception {
        return processEngine().getObject().getFormService();
    }

    //    <bean id="identityService" factory-bean="processEngine" factory-method="getIdentityService"/>
    @Bean
    public IdentityService identityService() throws Exception {
        return processEngine().getObject().getIdentityService();
    }
//    <bean id="taskService" factory-bean="processEngine" factory-method="getTaskService"/>

    @Bean
    public TaskService taskService() throws Exception {
        return processEngine().getObject().getTaskService();
    }
//    <bean id="historyService" factory-bean="processEngine" factory-method="getHistoryService"/>

    @Bean
    public HistoryService historyService() throws Exception {
        return processEngine().getObject().getHistoryService();
    }

    //    <bean id="managementService" factory-bean="processEngine" factory-method="getManagementService"/>
    @Bean
    public ManagementService managementService() throws Exception {
        return processEngine().getObject().getManagementService();
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }


    @Bean
    public RepositoryService RepositoryService() {
        return new RepositoryServiceImpl();
    }
}
