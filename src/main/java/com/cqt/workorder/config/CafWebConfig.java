/**
 * @copyright Copyright 1999-2017 © 99.com All rights reserved.
 * @license http://www.99.com/about
 */
package com.cqt.workorder.config;

import com.cqt.a8.rest.config.CafWebMvcConfigurerAdapter;
import org.springframework.context.annotation.*;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


/**
 * The type Caf web config.
 */
@Configuration
@EnableWebMvc
@ComponentScan(
        value = {"com.cqt.workorder.web","com.cqt.workorder.api","com.cqt.workorder.dao","org.activiti.rest.editor.model", "com.cqt.workorder.service"}
)
public class CafWebConfig extends CafWebMvcConfigurerAdapter {
    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/*.html", "/*.htm", "/*.jsp", "/*.js","/*.png").addResourceLocations("/");
        registry.addResourceHandler("/editor-app/**").addResourceLocations("/editor-app/");
//        registry.addResourceHandler("/lib/**").addResourceLocations("/lib/");
//        registry.addResourceHandler("/res/**").addResourceLocations("/res/");
        super.addResourceHandlers(registry);
    }
}