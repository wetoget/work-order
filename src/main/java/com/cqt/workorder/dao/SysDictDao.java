package com.cqt.workorder.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.cqt.workorder.domain.SysDict;

@Mapper
public interface SysDictDao {
    int insert(@Param("pojo") SysDict pojo);

    int insertSelective(@Param("pojo") SysDict pojo);

    int insertList(@Param("pojos") List<SysDict> pojo);

    int update(@Param("pojo") SysDict pojo);

    SysDict findByTypeAndValue(@Param("type")String type,@Param("value")String value);






}
