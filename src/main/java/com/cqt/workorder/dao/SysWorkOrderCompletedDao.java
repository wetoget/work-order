package com.cqt.workorder.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import com.cqt.workorder.domain.SysWorkOrderCompleted;

@Mapper
public interface SysWorkOrderCompletedDao {
    int insert(@Param("pojo") SysWorkOrderCompleted pojo);

    int insertSelective(@Param("pojo") SysWorkOrderCompleted pojo);

    int insertList(@Param("pojos") List<SysWorkOrderCompleted> pojo);

    int update(@Param("pojo") SysWorkOrderCompleted pojo);
}
