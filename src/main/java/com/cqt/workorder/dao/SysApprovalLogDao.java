package com.cqt.workorder.dao;

import com.cqt.workorder.domain.SysApprovalLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysApprovalLogDao {
    int insert(@Param("pojo") SysApprovalLog pojo);

    int insertSelective(@Param("pojo") SysApprovalLog pojo);

    int insertList(@Param("pojos") List<SysApprovalLog> pojo);

    int update(@Param("pojo") SysApprovalLog pojo);

    List<SysApprovalLog> findByWorkOrderId(@Param("workOrderId")String workOrderId);



}
