package com.cqt.workorder.dao;

import com.cqt.workorder.domain.SysWorkOrder;
import com.cqt.workorder.entity.PageParams;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysWorkOrderDao {
    int insert(@Param("pojo") SysWorkOrder pojo);

    int insertSelective(@Param("pojo") SysWorkOrder pojo);

    int insertList(@Param("pojos") List<SysWorkOrder> pojo);

    int update(@Param("pojo") SysWorkOrder pojo);

    int deleteByWorkOrderId(@Param("workOrderId")String workOrderId);

    SysWorkOrder findByWorkOrderId(@Param("workOrderId")String workOrderId);

    List<SysWorkOrder> find(@Param("offset")Integer offset, @Param("limit")Integer limit);

    List<SysWorkOrder> findByPageParams(@Param("pageParams") PageParams pageParams);

    Integer countByPageParams(@Param("pageParams") PageParams pageParams);

    Integer count();

}
