package com.cqt.workorder.dao;

import com.cqt.workorder.domain.SysDealLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysDealLogDao {
    int insert(@Param("pojo") SysDealLog pojo);

    int insertSelective(@Param("pojo") SysDealLog pojo);

    int insertList(@Param("pojos") List<SysDealLog> pojo);

    int update(@Param("pojo") SysDealLog pojo);

    List<SysDealLog> findByWorkOrderId(@Param("workOrderId")String workOrderId);

    List<SysDealLog> findByWorkOrderIdOrderByCreateTimeDesc(@Param("workOrderId")String workOrderId);




}
