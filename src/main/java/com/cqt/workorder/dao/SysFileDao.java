package com.cqt.workorder.dao;

import com.cqt.workorder.domain.SysFile;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysFileDao {
    int insert(@Param("pojo") SysFile pojo);

    int insertSelective(@Param("pojo") SysFile pojo);

    int insertList(@Param("pojos") List<SysFile> pojo);

    int update(@Param("pojo") SysFile pojo);

    SysFile findByDentryId(@Param("dentryId")String dentryId);


}
