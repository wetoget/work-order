package com.cqt.workorder.constants;

/**
 * 常量
 * Created by Su Sunbin on 2017/3/16.
 */
public interface Constants {

    //分页的默认值
    int DEFAULT_OFFSET = 0;//offset
    int DEFAULT_LIMIT = 20;//limit
    int DEFAULT_LIMIT_MAX = 100;//limit max value
    boolean DEFAULT_COUNT = false;//count


}
