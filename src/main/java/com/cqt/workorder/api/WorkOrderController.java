package com.cqt.workorder.api;

import com.cqt.workorder.domain.SysApprovalLog;
import com.cqt.workorder.domain.SysDealLog;
import com.cqt.workorder.domain.SysWorkOrder;
import com.cqt.workorder.domain.SysWorkOrderCompleted;
import com.cqt.workorder.entity.PageParams;
import com.cqt.workorder.service.SysApprovalLogService;
import com.cqt.workorder.service.SysDealLogService;
import com.cqt.workorder.service.SysWorkOrderService;
import com.cqt.workorder.validator.Create;
import com.cqt.workorder.validator.Modify;
import com.cqt.workorder.validator.PageValid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: work order
 * @description: word order controller
 * @author: wangmingkui
 * @create: 2018-05-15 16:56
 **/

@RestController
@RequestMapping("/v0.1/work_orders")
public class WorkOrderController {

    @Autowired
    SysWorkOrderService sysWorkOrderService;

    @Autowired
    private SysApprovalLogService sysApprovalLogService;

    @Autowired
    private SysDealLogService sysDealLogService;

    private static final Logger logger = LogManager.getLogger(WorkOrderController.class);

    @RequestMapping(value = "", method = RequestMethod.POST)
    public SysWorkOrder createWorkOrder(@RequestBody @Validated(value = Create.class) SysWorkOrder inputWorkOrder) throws Exception {
        SysWorkOrder sysWorkOrder = sysWorkOrderService.createWorkOrder(inputWorkOrder);
        return sysWorkOrder;
    }


    @RequestMapping(value = "", method = RequestMethod.GET)
    public Map<String, Object> getWorkOrders(@Validated(PageValid.class) PageParams requestParams,
                                             @RequestParam(value = "work_order_status", required = false) Integer workOrderStatus,
                                             @RequestParam(value = "ge_work_order_status", required = false) Integer geWorkOrderStatus,
                                             @RequestParam(value = "agreement_status", required = false) Integer agreementStatus,
                                             @RequestParam(value = "sl_special_line_operators", required = false) String slSpecialLineOperators,
                                             @RequestParam(value = "setup_fees_status", required = false) Boolean setupFeeStatus,
                                             @RequestParam(value = "enable_status", required = false) Boolean enableStatus,
                                             @RequestParam(value = "enable_time_start", required = false) Date enableTimeStart,
                                             @RequestParam(value = "enable_time_end", required = false) Date enableTimeEnd,
                                             @RequestParam(value = "due_date_start", required = false) Date dueDateStart,
                                             @RequestParam(value = "due_date_end", required = false) Date dueDateEnd,
                                             @RequestParam(value = "client_name", required = false) String clientName
    ) {

        if (null != workOrderStatus) {
            requestParams.setWorkOrderStatus(workOrderStatus);
        }
        if (null != geWorkOrderStatus) {
            requestParams.setGeWorkOrderStatus(geWorkOrderStatus);
        }
        if (null != agreementStatus) {
            requestParams.setAgreementStatus(agreementStatus);
        }
        if (null != slSpecialLineOperators) {
            requestParams.setSlSpecialLineOperators(slSpecialLineOperators);
        }
        if (null != setupFeeStatus) {
            requestParams.setSetupFeesStatus(setupFeeStatus);
        }
        if (null != enableStatus) {
            requestParams.setEnableStatus(enableStatus);
        }
        if (null != enableTimeStart) {
            requestParams.setEnableTimeStart(enableTimeStart);
        }
        if (null != enableTimeEnd) {
            requestParams.setEnableTimeEnd(enableTimeEnd);
        }
        if (null != dueDateStart) {
            requestParams.setDueDateStart(dueDateStart);
        }
        if (null != dueDateEnd) {
            requestParams.setDueDateEnd(dueDateEnd);
        }
        if (null != clientName) {
            requestParams.setClientName(clientName);
        }

        List<SysWorkOrder> sysWorkOrders = sysWorkOrderService.findByPageParams(requestParams);
        Map<String, Object> reMap = new HashMap<>();
        if (requestParams.getCount()) {
            int count = sysWorkOrderService.countByPageParams(requestParams);
            reMap.put("count", count);
        }
        reMap.put("items", sysWorkOrders);
        return reMap;
    }

    @RequestMapping(value = "/actions/export", method = RequestMethod.GET)
    public void exportRecords(HttpServletResponse response, @Validated(PageValid.class) PageParams requestParams,
                              @RequestParam(value = "work_order_status", required = false) Integer workOrderStatus,
                              @RequestParam(value = "sl_special_line_operators", required = false) String slSpecialLineOperators,
                              @RequestParam(value = "setup_fees_status", required = false) Boolean setupFeeStatus,
                              @RequestParam(value = "enable_status", required = false) Boolean enableStatus,
                              @RequestParam(value = "enable_time_start", required = false) Date enableTimeStart,
                              @RequestParam(value = "enable_time_end", required = false) Date enableTimeEnd,
                              @RequestParam(value = "due_date_start", required = false) Date dueDateStart,
                              @RequestParam(value = "due_date_end", required = false) Date dueDateEnd,
                              @RequestParam(value = "client_name", required = false) String clientName
    ) {

        if (null != workOrderStatus) {
            requestParams.setWorkOrderStatus(workOrderStatus);
        }
        if (null != slSpecialLineOperators) {
            requestParams.setSlSpecialLineOperators(slSpecialLineOperators);
        }
        if (null != setupFeeStatus) {
            requestParams.setSetupFeesStatus(setupFeeStatus);
        }
        if (null != enableStatus) {
            requestParams.setEnableStatus(enableStatus);
        }
        if (null != enableTimeStart) {
            requestParams.setEnableTimeStart(enableTimeStart);
        }
        if (null != enableTimeEnd) {
            requestParams.setEnableTimeEnd(enableTimeEnd);
        }
        if (null != dueDateStart) {
            requestParams.setDueDateStart(dueDateStart);
        }
        if (null != dueDateEnd) {
            requestParams.setDueDateEnd(dueDateEnd);
        }
        if (null != clientName) {
            requestParams.setClientName(clientName);
        }
        sysWorkOrderService.exportExcelByPageParams(requestParams, response);

    }

    @RequestMapping(value = "/{work_order_id}", method = RequestMethod.GET)
    public SysWorkOrder getWorkOrder(@PathVariable("work_order_id") String workOrderId) {
        SysWorkOrder sysWorkOrder = sysWorkOrderService.findByWorkOrderId(workOrderId);
        return sysWorkOrder;
    }

    @RequestMapping(value = "/{work_order_id}/actions/approval", method = RequestMethod.PATCH)
    public SysWorkOrder approvalWorkOrder(@RequestBody @Validated SysApprovalLog approvalLog, @PathVariable("work_order_id") String workOrderId) {
        SysWorkOrder sysWorkOrder = sysWorkOrderService.approvalWorkOrder(approvalLog, workOrderId);
        return sysWorkOrder;
    }

    @RequestMapping(value = "/{work_order_id}", method = RequestMethod.PATCH)
    public SysWorkOrder updateWorkOrder(@PathVariable("work_order_id") String workOrderId, @RequestBody @Validated(value = Modify.class) SysWorkOrder inputWorkOrder) {
        inputWorkOrder.setWorkOrderId(workOrderId);
        sysWorkOrderService.updateWorkOrder(inputWorkOrder);
        return inputWorkOrder;
    }

    @RequestMapping(value = "/{work_order_id}", method = RequestMethod.DELETE)
    public SysWorkOrder deleteWorkOrder(@PathVariable("work_order_id") String workOrderId) {
        return sysWorkOrderService.deleteWorkOrder(workOrderId);

    }

    @RequestMapping(value = "/{work_order_id}/approval_logs", method = RequestMethod.GET)
    public Map<String, Object> getApprovalLogs(@PathVariable("work_order_id") String workOrderId) {
        List<SysApprovalLog> list = sysApprovalLogService.findByWorkOrderId(workOrderId);
        Map<String, Object> reMap = new HashMap<>();
        reMap.put("items", list);
        return reMap;
    }

    @RequestMapping(value = "/{work_order_id}/actions/deal", method = RequestMethod.PATCH)
    public SysWorkOrder dealWorkOrder(@RequestBody @Validated SysDealLog sysDealLog, @PathVariable("work_order_id") String workOrderId) throws Exception {
        SysWorkOrder sysWorkOrder = sysWorkOrderService.dealWorkOrder(sysDealLog, workOrderId);
        return sysWorkOrder;
    }

    @RequestMapping(value = "/{work_order_id}/deal_logs", method = RequestMethod.GET)
    public Map<String, Object> getDealLogsLogs(@PathVariable("work_order_id") String workOrderId) {
        List<SysDealLog> list = sysDealLogService.findByWorkOrderId(workOrderId);
        Map<String, Object> reMap = new HashMap<>();
        reMap.put("items", list);
        return reMap;
    }


//    @RequestMapping(value = "/{work_order_id}/due_date", method = RequestMethod.PATCH)
//    public MonthRentalLog updateDueDate(@RequestBody MonthRentalLog monthRentalLog, @PathVariable("work_order_id") String workOrderId) {
//        //SysWorkOrder sysWorkOrder = sysWorkOrderService.findByWorkOrderId(workOrderId);
//        //monthRentalLog.setDueDate();
//        //monthRentalLog.setPayTime();
//        monthRentalLog.setWorkOrderId(workOrderId);
//        monthRentalLog.setMonthRentalId(StringUtil.getUUID());
//        //monthRentalLogService.findByWorkOrderIdOrderByPayTimeDesc(workOrderId);
//        monthRentalLogService.insertSelective(monthRentalLog);
//        return monthRentalLog;
//    }

//    @RequestMapping(value = "/{work_order_id}/month_rental_logs", method = RequestMethod.GET)
//    public Map<String, Object> getMonthRentalLogs(@PathVariable("work_order_id") String workOrderId) {
//        List<MonthRentalLog> list = monthRentalLogService.findByWorkOrderIdOrderByPayTimeDesc(workOrderId);
//        Map<String, Object> reMap = new HashMap<>();
//        reMap.put("items", list);
//        return reMap;
//    }

    @RequestMapping(value = "/{work_order_id}/enable_status", method = RequestMethod.PATCH)
    public SysWorkOrderCompleted updateEnableStatus(@RequestBody @Validated  SysWorkOrderCompleted sysWorkOrderCompleted, @PathVariable("work_order_id") String workOrderId) {
        sysWorkOrderCompleted.setWorkOrderId(workOrderId);
        sysWorkOrderCompleted.setEnableTime(new Timestamp(System.currentTimeMillis()));
        SysWorkOrderCompleted reWorkOrderCompleted = sysWorkOrderService.updateEnableStatus(sysWorkOrderCompleted, workOrderId);
        return reWorkOrderCompleted;
    }


}
