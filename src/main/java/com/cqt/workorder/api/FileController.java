package com.cqt.workorder.api;
import com.cqt.workorder.domain.SysFile;

import com.cqt.workorder.exception.CustomException;
import com.cqt.workorder.service.SysFileService;
import com.cqt.workorder.util.StringUtil;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Timestamp;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@PropertySource("classpath:app.properties")
@RestController
@RequestMapping("/v0.1")
public class FileController {
    @Value("${file.preAbsoluteFolder}")
    private String preAbsoluteFolder;

    @Autowired
    SysFileService sysFileService;

    @RequestMapping(value = "/files", method = RequestMethod.POST)
    public SysFile upload(@RequestParam(value = "file") MultipartFile inputFile,
                          @RequestParam(value = "file_name") String inputFileName) throws Exception {

        //如果文件不为空，写入上传路径
        SysFile sysFile=new SysFile();
        if(!inputFile.isEmpty()&&!inputFileName.isEmpty()) {

            //String FileNam_REG_EX = "[/:*?\"<>|]{1}";
            Pattern p = Pattern.compile("[/:*?\"<>|]+");
            Matcher m = p.matcher(inputFileName); // 获取 matcher 对象
            if(m.find()) {
                throw new CustomException(HttpStatus.BAD_REQUEST, "FileName.invalid");
            }

            //使用默认时区和语言环境获得一个日历。
            Calendar rightNow    =    Calendar.getInstance();
            /*用Calendar的get(int field)方法返回给定日历字段的值。
            HOUR 用于 12 小时制时钟 (0 - 11)，HOUR_OF_DAY 用于 24 小时制时钟。*/
            Integer year = rightNow.get(Calendar.YEAR);
            Integer month = rightNow.get(Calendar.MONTH)+1; //第一个月从0开始，所以得到月份＋1
            //Integer day = rightNow.get(rightNow.DAY_OF_MONTH);

            //上传文件路径
            //String preAbsoluteFolder="D:"+File.separator;
            String preFilePath="files"+File.separator+year+month;
            String absoluteFolder=preAbsoluteFolder+preFilePath;

            //上传文件名
            String filename = inputFile.getOriginalFilename();
            File file = new File(absoluteFolder,filename);

            //判断路径是否存在，如果不存在就创建一个
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            //将上传文件保存到一个目标文件当中
            String dentryId= StringUtil.getUUID();
            String targetName=dentryId+"."+getExtName(file);
            String targetPath=absoluteFolder + File.separator + targetName;
            String filePath=preFilePath + File.separator + targetName;
            inputFile.transferTo(new File(targetPath));
            Timestamp curTime = new Timestamp(System.currentTimeMillis());

            sysFile.setDentryId(dentryId);
            sysFile.setOriginalFilename(inputFile.getOriginalFilename());
            sysFile.setFileName(inputFileName);
            sysFile.setPath(filePath);
            sysFile.setCreateTime(curTime);
            sysFileService.insertSelective(sysFile);
        } else {
            throw new CustomException(HttpStatus.NOT_FOUND, "invalid.argument");
        }

//        Map map = new HashMap();
//        map.put("items", "");
        return sysFile;
    }

    @RequestMapping(value = "/files/{dentry_id} ", method = RequestMethod.GET)
    public ResponseEntity<byte[]> download(@PathVariable("dentry_id") String dentryId,HttpServletRequest request) throws Exception {
        //下载文件路径
        //String preAbsoluteFolder="D:"+File.separator;
        SysFile sysFile=sysFileService.findByDentryId(dentryId);
        if(null==sysFile){
            throw new CustomException(HttpStatus.BAD_REQUEST, "DENTRY_ID_NOT_FOUND");
        }
        File file = new File(preAbsoluteFolder+sysFile.getPath());
        HttpHeaders headers = new HttpHeaders();
        //下载显示的文件名，解决中文名称乱码问题

        //String downloadFielName = new String(file.getName().getBytes("UTF-8"),"iso-8859-1");
        String downloadFielName =sysFile.getFileName();
        //通知浏览器以attachment（下载方式）打开图片
        headers.setContentDispositionFormData("attachment", downloadFielName);
        //application/octet-stream ： 二进制流数据（最常见的文件下载）。
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file),
                headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/images/{dentry_id} ", method = RequestMethod.GET)
    public ResponseEntity<byte[]> getImage(@PathVariable("dentry_id") String dentryId,HttpServletRequest request) throws Exception {
        //下载文件路径
        //String preAbsoluteFolder="D:"+File.separator;
        SysFile sysFile=sysFileService.findByDentryId(dentryId);
        if(null==sysFile){
            throw new CustomException(HttpStatus.BAD_REQUEST, "DENTRY_ID_NOT_FOUND");
        }
        File file = new File(preAbsoluteFolder+sysFile.getPath());
        HttpHeaders headers = new HttpHeaders();
        //下载显示的文件名，解决中文名称乱码问题

        //String downloadFielName = new String(file.getName().getBytes("UTF-8"),"iso-8859-1");
        //String downloadFielName =sysFile.getFileName();
        //通知浏览器以attachment（下载方式）打开图片
        //headers.setContentDispositionFormData("attachment", downloadFielName);
        //application/octet-stream ： 二进制流数据（最常见的文件下载）。
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file),
                headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/images2/{dentry_id} ", method = RequestMethod.GET)
    public String  getImage(@PathVariable("dentry_id") String dentryId,HttpServletRequest request,HttpServletResponse response) throws Exception {
        //下载文件路径
        //String preAbsoluteFolder="D:"+File.separator;
        SysFile sysFile=sysFileService.findByDentryId(dentryId);
        if(null==sysFile){
            throw new CustomException(HttpStatus.BAD_REQUEST, "DENTRY_ID_NOT_FOUND");
        }
        ServletOutputStream out = null;
        FileInputStream ips = null;
        try {
            File file = new File(preAbsoluteFolder+sysFile.getPath());
            //获取图片存放路径
            ips = new FileInputStream(file);
            response.setContentType("multipart/form-data");
            out = response.getOutputStream();
            //读取文件流
            int len = 0;
            byte[] buffer = new byte[1024 * 10];
            while ((len = ips.read(buffer)) != -1){
                out.write(buffer,0,len);
            }
            out.flush();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            out.close();
            ips.close();
        }
        return null;
    }

    private String getExtName(File file) {
        String fileName = file.getName();
        return fileName.substring(fileName.lastIndexOf(".") + 1);

    }

}
