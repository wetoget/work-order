package com.cqt.workorder.api;
import com.cqt.workorder.exception.CustomException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("")
public class CafDemoApiController {
    @RequestMapping(method = RequestMethod.GET)
    public Object demo() {

        throw new CustomException(HttpStatus.BAD_REQUEST, "username.null");
//        Map map=new HashMap();
//        map.put("name","hello world");
//        return map;
    }

}
