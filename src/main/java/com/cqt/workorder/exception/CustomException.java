
package com.cqt.workorder.exception;

import com.cqt.a8.restclient.SR;
import com.cqt.a8.restclient.WafException;
import org.springframework.http.HttpStatus;

/**
 * The type Custom exception.
 *
 */
public class CustomException extends WafException {

    /**
     * The constant PRE_CODE.
     */
    public static final String PRE_CODE = "WO/";

    /**
     * Instantiates a new Custom exception.
     *
     * @param status  the status
     * @param code    the code
     * @param message the message
     * @param cause   the cause
     */
    public CustomException(HttpStatus status, String code, String message, Throwable cause) {
        super(PRE_CODE + code.toUpperCase().replaceAll("\\.", "_"), message, status, cause);
    }

    /**
     * Instantiates a new Custom exception.(不支持多国语言)
     *
     * @param status  the status
     * @param code    the code
     * @param message the message
     */
    public CustomException(HttpStatus status, String code, String message) {
        super(PRE_CODE + code.toUpperCase().replaceAll("\\.", "_"), message, status);
    }

    /**
     * Instantiates a new Custom exception.
     *
     * @param status the status
     * @param code   the code
     */
    public CustomException(HttpStatus status, String code) {
        this(status, code, SR.format(code));
    }

    /**
     * Instantiates a new Custom exception.
     *
     * @param status the status
     * @param code   the code
     * @param values the values
     */
    public CustomException(HttpStatus status, String code, Object... values) {
        this(status, code, SR.format(code, values));
    }

    /**
     * Instantiates a new Custom exception.
     *
     * @param code the code
     */
    public CustomException(String code) {
        this(HttpStatus.BAD_REQUEST, code, SR.format(code));
    }

    /**
     * Instantiates a new Custom exception.
     *
     * @param code   the code
     * @param values the values
     */
    public CustomException(String code, Object... values) {
        this(HttpStatus.BAD_REQUEST, code, SR.format(code, values));
    }

    /**
     * Instantiates a new Custom exception.
     *
     * @param code    the code
     * @param message the message
     */
    public CustomException(String code, String message) {
        this(HttpStatus.BAD_REQUEST, code, message);
    }

    /**
     * Instantiates a new Custom exception.
     *
     * @param code  the code
     * @param cause the cause
     */
    public CustomException(String code, Throwable cause) {
        this(HttpStatus.BAD_REQUEST, code, SR.format(code), cause);
    }

    /**
     * Instantiates a new Custom exception.
     *
     * @param status the status
     * @param code   the code
     * @param cause  the cause
     */
    public CustomException(HttpStatus status, String code, Throwable cause) {
        this(status, code, SR.format(code), cause);
    }

    /**
     * Gets status.
     *
     * @return the status
     */
    public HttpStatus getStatus() {
        return super.getResponseEntity().getStatusCode();
    }
}
