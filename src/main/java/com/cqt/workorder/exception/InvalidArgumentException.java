package com.cqt.workorder.exception;

import com.cqt.a8.restclient.SR;
import com.cqt.a8.restclient.WafException;
import org.springframework.http.HttpStatus;

/**
 * 在程序中手动判断参数时抛出的参数异常
 */
public class InvalidArgumentException extends WafException {
    /**
     * The constant INVALID_ARGUMENT.
     */
    public static final String INVALID_ARGUMENT = "INVALID_ARGUMENT";

    /**
     * Instantiates a new Invalid argument exception.
     *
     * @param code the code
     */
    public InvalidArgumentException(String code) {
        super(CustomException.PRE_CODE + INVALID_ARGUMENT, SR.format(code), HttpStatus.BAD_REQUEST);
    }

}
