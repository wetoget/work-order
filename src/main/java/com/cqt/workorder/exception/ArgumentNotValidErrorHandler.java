
package com.cqt.workorder.exception;

import com.cqt.a8.rest.exceptions.rest.AbstractRestErrorHandler;
import com.cqt.a8.restclient.SR;
import com.cqt.a8.restclient.exception.entity.ResponseErrorMessage;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;
import java.util.UnknownFormatConversionException;

import static com.cqt.workorder.exception.CustomException.PRE_CODE;
import static com.cqt.workorder.exception.InvalidArgumentException.INVALID_ARGUMENT;


/**
 * The type Argument not valid error handler.
 *
 */
public class ArgumentNotValidErrorHandler extends AbstractRestErrorHandler {

    /**
     * Process response entity.
     *
     * @param throwable the throwable
     * @param request   the request
     * @return the response entity
     */
    @Override
    public ResponseEntity<ResponseErrorMessage> process(Throwable throwable, HttpServletRequest request) {
        ResponseErrorMessage errorMessage = getBody(throwable, request);
        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

    /**
     * Gets body.
     *
     * @param throwable the throwable
     * @param request   the request
     * @return the body
     */
    @Override
    protected ResponseErrorMessage getBody(Throwable throwable, HttpServletRequest request) {
        FieldError fieldError;
        String messageCode = null;
        Object[] arguments = null;
        if ((MethodArgumentNotValidException.class).isInstance(throwable)) {
            MethodArgumentNotValidException validException = ((MethodArgumentNotValidException) throwable);
            fieldError = validException.getBindingResult().getFieldError();
            messageCode = fieldError.getDefaultMessage();
            arguments = fieldError.getArguments();
        } else if (BindException.class.isInstance(throwable)) {
            BindException bindException = (BindException) throwable;
            fieldError = bindException.getBindingResult().getFieldError();
            messageCode = fieldError.getDefaultMessage();
            arguments = fieldError.getArguments();
        } else if (ConstraintViolationException.class.isInstance(throwable)) {
            ConstraintViolationException constraintViolationException = (ConstraintViolationException) throwable;
            Set<ConstraintViolation<?>> constraintViolations = constraintViolationException.getConstraintViolations();
            for (ConstraintViolation<?> constraintViolation : constraintViolations) {
                messageCode = constraintViolation.getMessage();
                if (StringUtils.isNotBlank(messageCode)) {
                    break;
                }
            }
            arguments = null;
        }

        ResponseErrorMessage errorMessage = new ResponseErrorMessage(throwable);
        String message;
        if (messageCode == null) {
            message = throwable.getLocalizedMessage();
        } else {
            String defaultMessage = messageCode.replaceAll("\\{|\\}", "");
//            Object[] arguments = fieldError.getArguments();
            if (arguments != null) {
                ArrayUtils.reverse(arguments);
            }
            try {
                message = SR.format(request.getLocale(), defaultMessage, arguments);
            } catch (UnknownFormatConversionException ex) {
                message = defaultMessage;
            }
        }

        errorMessage.setMessage(message);
        errorMessage.setCode(PRE_CODE + INVALID_ARGUMENT);
        errorMessage.setDetail(appendStackTrace(null, throwable));
        updateRemoteErrorMessage(errorMessage, request);

        return errorMessage;
    }
}
